
import 'package:flutter/material.dart';

class FlagInd extends StatelessWidget {
  const FlagInd({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Flag of India",
        style:TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.black,
          ),
        ),
        backgroundColor: Colors.orange,
      ),
      backgroundColor:Colors.black,
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              children: [
                const Padding(padding: EdgeInsets.only(top: 208)),
                Container(
                  height: 400,
                  width: 3,
                  color: Colors.white,
                ),
              ],
            ),
            
            Column(
              children: [
                const Padding(padding: EdgeInsets.only(top: 208)),
                Container(
                  height: 70,
                  width: 400,
                  color: Colors.orange,
                ),
                Container(
                  height: 70,
                  width: 400,
                  color: Colors.white,
                  child: Image.network(
                        "https://media.istockphoto.com/id/1182479181/vector/ashoka-chakra.jpg?s=612x612&w=0&k=20&c=bDrv1c_tw5pOa-R0Agg62ZjeMJl3S22Trk-ViEhJiIU=",
                        height: 70,
                        width: 70,
                      ),
                ),Container(
                  height: 70,
                  width: 400,
                  color: Colors.green,
                ),
              ],
            ),
          ],
          
        ),
      ),
    );
  }
}