import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

var database;

Future insertPlayerData(Player obj)async{
  final localDB = await database;

  await localDB.insert(
    "Player",
    obj.playerMap(),
    conflictAlgorithm:ConflictAlgorithm.replace,
  );
}

Future <List <Player>>getPlayerData()async{
  final localDB = await database;

  List<Map<String,dynamic>> listPlayer = await localDB.query("Player");

  return List.generate(listPlayer.length, (index){
    return Player(
      name: listPlayer[index]['name'],
      jerNo: listPlayer[index]['jerNo'],
      runs: listPlayer[index]['runs'],
      avg: listPlayer[index]['avg'],
    );
  });
}

class Player {
  final String name;
  final int jerNo;
  final int runs;
  final double avg;

  Player({
    required this.name,
    required this.jerNo,
    required this.runs,
    required this.avg,
  });

  Map<String, dynamic> playerMap (){
    return {
      'name': name,
      'jerNo': jerNo,
      'runs': runs,
      'avg': avg,
    };
  }

  @override
  String toString(){
    return '{name:$name, jerNo: $jerNo, runs: $runs, avg: $avg}';
  }
}
void main() async{
  runApp(const MainApp());

  database = openDatabase(
    join(await getDatabasesPath(),"playerDB.db"),
    version: 1,
    onCreate: (db, version) {
      db.execute(
        '''CREATE TABLE Player(
          name TEXT,
          jerNo INTEGER PRIMARY KEY,
          runs INT,
          avg REAL
        )'''
      );
    },
  );
  // insert into

  Player batsman1 = Player(
    name: "Virat", 
    jerNo: 18, 
    runs: 13000, 
    avg: 53.66,
  );
  await insertPlayerData(batsman1);
  print(await getPlayerData());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body: Center(
          child: Text('Hello World!'),
        ),
      ),
    );
  }
}
