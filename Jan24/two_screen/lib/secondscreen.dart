
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

class SecondScreen extends StatefulWidget {
  const SecondScreen({super.key});

  @override
  State createState()=> _SecondScreenState();
}

class _SecondScreenState extends State {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Expanded(
        child: Container(
          width: double.infinity,
          decoration:const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.center,
              stops: [0.01,0.8],
              colors: [
                Color.fromRGBO(197, 4, 98, 1),
                Color.fromRGBO(80, 3, 112, 1)
              ],
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 89,
              ),

              // Sized Box for Text

              Padding(
                padding: const EdgeInsets.only(left: 38),
                child: SizedBox(
                  height: 151,
                  width: 278,
                  child: Column(
                    children: [
                      Text("UX Designer from \nScratch.",
                        style: GoogleFonts.jost(
                          fontWeight:FontWeight.w500,
                          fontSize:32.61,
                          color: const Color.fromRGBO(255,255,255,1),
                        ),
                      ),
                      const SizedBox(
                        height: 11,
                      ),
                      Text("Basic guideline & tips & tricks for how to become a UX designer easily.",
                        style: GoogleFonts.jost(
                          fontWeight:FontWeight.w400,
                          fontSize:16,
                          color: const Color.fromRGBO(228, 205, 225, 1),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 38,top: 27),
                child: Row(
                  children: [
                    Image.asset(
                      "assets/images/person.png",
                    ),
                    const SizedBox(
                      width: 7,
                    ),
                    Text("Author: ",
                      style: GoogleFonts.jost(
                        fontWeight:FontWeight.w400,
                        fontSize:16,
                        color:const Color.fromRGBO(190, 154, 197, 1),
                      ),
                    ),
                    
                    Text("Jenny",
                      style: GoogleFonts.jost(
                        fontWeight:FontWeight.w500,
                        fontSize:16,
                        color:const Color.fromRGBO(255,255,255, 1),
                      ),
                    ),
                    const SizedBox(
                      width: 55,
                    ),
                    Text("4.8",
                      style: GoogleFonts.jost(
                        fontWeight:FontWeight.w500,
                        fontSize:16,
                        color: const Color.fromRGBO(255,255,255,1),
                      ),
                    ),
                    Image.asset(
                      "assets/images/star.png",
                      height: 20,
                      width: 20,
                    ),
                    Text("(20 review)",
                      style: GoogleFonts.jost(
                        fontWeight:FontWeight.w400,
                        fontSize:16,
                        color: const Color.fromRGBO(190, 154, 197, 1),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 32,
              ),

              // Container White
              Expanded(
                child: Container(
                  width: double.infinity,
                  decoration: const BoxDecoration(
                    color: Color.fromRGBO(255,255,255,1),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(38),
                      topRight: Radius.circular(38)
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Multiple Container in 1 Container
                      // 1
                      Padding(
                        padding: const EdgeInsets.only(left: 30,right: 30,top: 30),
                        child: Container(
                          height: 70,
                          width: 350,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: Color.fromRGBO(255,255,255,1),
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 8),
                                blurRadius: 40,
                                color: Color.fromRGBO(0,0,0,0.15),
                              ),
                            ],
                          ),
                          child: Row(
                            children: [
                              Container(
                                height: 60,
                                width: 46,
                                margin: const EdgeInsets.all(5),
                                decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(12),
                                  ),
                                color: Color.fromRGBO(230, 239, 239, 1),
                                ),
                                child: Image.asset(
                                  "assets/images/youtube.png",
                                ),
                              ),

                              // Column in Container
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    width: 6,
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 11,top: 12),
                                      child: SizedBox(
                                        width: 95,
                                        child: Text("Introduction",
                                          style: GoogleFonts.jost(
                                            fontWeight:FontWeight.w500,
                                            fontSize:17,
                                            color:const Color.fromRGBO(0,0,0,1),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 11,top: 2),
                                      child: SizedBox(
                                        width: 200,
                                        child: Text("Lorem Ipsum is simply dummy text ... ",
                                          style: GoogleFonts.jost(
                                            fontWeight:FontWeight.w400,
                                            fontSize:12,
                                            color:const Color.fromRGBO(143,143,143,1),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),

                      // 2
                      Padding(
                        padding: const EdgeInsets.only(left: 30,right: 30,top: 20),
                        child: Container(
                          height: 70,
                          width: 350,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: Color.fromRGBO(255,255,255,1),
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 8),
                                blurRadius: 40,
                                color: Color.fromRGBO(0,0,0,0.15),
                              ),
                            ],
                          ),
                          child: Row(
                            children: [
                              Container(
                                height: 60,
                                width: 46,
                                margin: const EdgeInsets.all(5),
                                decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(12),
                                  ),
                                color: Color.fromRGBO(230, 239, 239, 1),
                                ),
                                child: Image.asset(
                                  "assets/images/youtube.png",
                                ),
                              ),

                              // Column in Container
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    width: 6,
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 11,top: 12),
                                      child: SizedBox(
                                        width: 95,
                                        child: Text("Introduction",
                                          style: GoogleFonts.jost(
                                            fontWeight:FontWeight.w500,
                                            fontSize:17,
                                            color:const Color.fromRGBO(0,0,0,1),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 11,top: 2),
                                      child: SizedBox(
                                        width: 200,
                                        child: Text("Lorem Ipsum is simply dummy text ... ",
                                          style: GoogleFonts.jost(
                                            fontWeight:FontWeight.w400,
                                            fontSize:12,
                                            color:const Color.fromRGBO(143,143,143,1),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),

                      // 3
                      Padding(
                        padding: const EdgeInsets.only(left: 30,right: 30,top: 20),
                        child: Container(
                          height: 70,
                          width: 350,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: Color.fromRGBO(255,255,255,1),
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 8),
                                blurRadius: 40,
                                color: Color.fromRGBO(0,0,0,0.15),
                              ),
                            ],
                          ),
                          child: Row(
                            children: [
                              Container(
                                height: 60,
                                width: 46,
                                margin: const EdgeInsets.all(5),
                                decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(12),
                                  ),
                                color: Color.fromRGBO(230, 239, 239, 1),
                                ),
                                child: Image.asset(
                                  "assets/images/youtube.png",
                                ),
                              ),

                              // Column in Container
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    width: 6,
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 11,top: 12),
                                      child: SizedBox(
                                        width: 95,
                                        child: Text("Introduction",
                                          style: GoogleFonts.jost(
                                            fontWeight:FontWeight.w500,
                                            fontSize:17,
                                            color:const Color.fromRGBO(0,0,0,1),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 11,top: 2),
                                      child: SizedBox(
                                        width: 200,
                                        child: Text("Lorem Ipsum is simply dummy text ... ",
                                          style: GoogleFonts.jost(
                                            fontWeight:FontWeight.w400,
                                            fontSize:12,
                                            color:const Color.fromRGBO(143,143,143,1),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      
                      // 4
                      Padding(
                        padding: const EdgeInsets.only(left: 30,right: 30,top: 20),
                        child: Container(
                          height: 70,
                          width: 350,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: Color.fromRGBO(255,255,255,1),
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 8),
                                blurRadius: 40,
                                color: Color.fromRGBO(0,0,0,0.15),
                              ),
                            ],
                          ),
                          child: Row(
                            children: [
                              Container(
                                height: 60,
                                width: 46,
                                margin: const EdgeInsets.all(5),
                                decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(12),
                                  ),
                                color: Color.fromRGBO(230, 239, 239, 1),
                                ),
                                child: Image.asset(
                                  "assets/images/youtube.png",
                                ),
                              ),

                              // Column in Container
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    width: 6,
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 11,top: 12),
                                      child: SizedBox(
                                        width: 95,
                                        child: Text("Introduction",
                                          style: GoogleFonts.jost(
                                            fontWeight:FontWeight.w500,
                                            fontSize:17,
                                            color:const Color.fromRGBO(0,0,0,1),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 11,top: 2),
                                      child: SizedBox(
                                        width: 200,
                                        child: Text("Lorem Ipsum is simply dummy text ... ",
                                          style: GoogleFonts.jost(
                                            fontWeight:FontWeight.w400,
                                            fontSize:12,
                                            color:const Color.fromRGBO(143,143,143,1),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),

                      // 5
                      Padding(
                        padding: const EdgeInsets.only(left: 30,right: 30,top: 20),
                        child: Container(
                          height: 70,
                          width: 350,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: Color.fromRGBO(255,255,255,1),
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 8),
                                blurRadius: 40,
                                color: Color.fromRGBO(0,0,0,0.15),
                              ),
                            ],
                          ),
                          child: Row(
                            children: [
                              Container(
                                height: 60,
                                width: 46,
                                margin: const EdgeInsets.all(5),
                                decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(12),
                                  ),
                                color: Color.fromRGBO(230, 239, 239, 1),
                                ),
                                child: Image.asset(
                                  "assets/images/youtube.png",
                                ),
                              ),

                              // Column in Container
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    width: 6,
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 11,top: 12),
                                      child: SizedBox(
                                        width: 95,
                                        child: Text("Introduction",
                                          style: GoogleFonts.jost(
                                            fontWeight:FontWeight.w500,
                                            fontSize:17,
                                            color:const Color.fromRGBO(0,0,0,1),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 11,top: 2),
                                      child: SizedBox(
                                        width: 200,
                                        child: Text("Lorem Ipsum is simply dummy text ... ",
                                          style: GoogleFonts.jost(
                                            fontWeight:FontWeight.w400,
                                            fontSize:12,
                                            color:const Color.fromRGBO(143,143,143,1),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}