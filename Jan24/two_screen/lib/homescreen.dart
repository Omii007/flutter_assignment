
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:two_screen/secondscreen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State createState()=> _HomeScreenState();
}

class _HomeScreenState extends State {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(205, 218, 218, 1),
      body: Column(
        children: [
          const SizedBox(
            height: 47,
          ),

          //Top two Icons

          Padding(
            padding: const EdgeInsets.only(left: 20,right: 20),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    
                  },
                  child: Image.asset(
                    "assets/images/menu.png",
                    width: 26,
                    height: 26,
                  ),
                ),
                const Spacer(),
                GestureDetector(
                  onTap: () {
                    
                  },
                  child: Image.asset(
                    "assets/images/bell.png",
                    height: 26,
                    width: 26,
                  ),
                ),
              ],
            ),
          ),

          // SizedBox for 2 Text

          Padding(
              padding: const EdgeInsets.only(left: 20),
              child: SizedBox(
                width: double.infinity,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text("Welcome to New",
                          style: GoogleFonts.jost(
                            fontWeight: FontWeight.w300,
                            fontSize: 27,
                            color:const Color.fromRGBO(0,0,0,1),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text("Educourse",
                          style: GoogleFonts.jost(
                            fontWeight: FontWeight.w700,
                            fontSize: 37,
                            color:const Color.fromRGBO(0,0,0,1),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          
          const SizedBox(
            height: 15,
          ),

          // TextField 

          Padding(
            padding: const EdgeInsets.only(left: 20,right: 20),
            child: TextField(             
              decoration: InputDecoration(
                enabled: false,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(28.5),
                ),
                fillColor: const Color.fromRGBO(255,255,255,1),
                suffixIcon: const Icon(Icons.search,
                  size: 30,
                  color: Color.fromRGBO(0,0,0,1),
                ),
                filled: true,
                hintText: "Enter your Keyword",
                hintStyle: GoogleFonts.inter(
                  fontWeight:FontWeight.w400,
                  fontSize:14,
                  color: const Color.fromRGBO(143, 143, 143, 1),
                ),
              ),
            ),
          ),
          
          // White Container Color

          Expanded(
            child: Container(
              margin: const EdgeInsets.only(top: 30),
              width: double.infinity,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(38),
                  topRight: Radius.circular(38),
                  ),
                  color: Color.fromRGBO(255,255,255,1),
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 23,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text("Course For You",
                            style: GoogleFonts.jost(
                              fontWeight:FontWeight.w500,
                              fontSize: 18,
                              color:const Color.fromRGBO(0,0,0,1),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),

                    // Row for multiple Container

                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row( 
                        
                        children: [

                          // 1st Container in row

                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context)=> const SecondScreen(),
                                ),
                              );
                            },
                            child: Container(
                              height: 242,
                              width: 190,
                              margin: const EdgeInsets.only(left: 20),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14),
                                gradient: const LinearGradient(
                                  colors: [
                                    Color.fromRGBO(197, 4, 98, 1),
                                    Color.fromRGBO(80, 3, 112, 1),
                                  ],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                ),
                              ),
                              
                              child: Column(
                                children: [
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  SizedBox(
                                    height: 50,
                                    width: 150,
                                    child: Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text("UX Designer from \nScratch",
                                              style: GoogleFonts.jost(
                                                fontWeight:FontWeight.w500,
                                                fontSize:17,
                                                color:const Color.fromRGBO(255,255,255,1),
                                              ),
                                            )
                                          ],
                                        ),
                                        
                                      ],
                                    )
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: SizedBox(
                                      height: 160,
                                       width: 160,
                                      child: Image.asset(
                                        "assets/images/container1.png",             
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),

                          // 2nd Container in the row

                          GestureDetector(
                            child: Container(
                              height: 242,
                              width: 190,
                              margin: const EdgeInsets.only(left: 20),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(14),
                                gradient: const LinearGradient(
                                  colors: [
                                    Color.fromRGBO(0, 77, 228, 1),
                                    Color.fromRGBO(1, 47, 135, 1),
                                  ],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                ),
                              ),
                              child: Column(
                                children: [
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  SizedBox(
                                    height: 50,
                                    width: 150,
                                    child: Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text("Design Thinking \nThe Beginner",
                                              style: GoogleFonts.jost(
                                                fontWeight:FontWeight.w500,
                                                fontSize:17,
                                                color:const Color.fromRGBO(255,255,255,1),
                                              ),
                                            )
                                          ],
                                        ),
                                        
                                      ],
                                    )
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: SizedBox(
                                      height: 160,
                                       width: 160,
                                      child: Image.asset(
                                        "assets/images/container2.png",          
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20,top: 30),
                      child: Row(
                        children: [
                          Text("Course By Category",
                            style: GoogleFonts.jost(
                              fontWeight:FontWeight.w500,
                              fontSize: 18,
                              color:const Color.fromRGBO(0,0,0,1),
                            ),
                          ),
                        ],
                      ),
                    ),

                    // Bottom images 4 in row
                    
                    Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GestureDetector(
                            onTap:() {
                              
                            },
                            child: Column(
                              children: [
                                Container(
                                  height: 36,
                                  width: 36,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: const Color.fromRGBO(25, 0, 56, 1)
                                  ),
                                  child: Image.asset(
                                    "assets/images/traced1.png"
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Text("UI/UX",
                                    style: GoogleFonts.jost(
                                      fontWeight:FontWeight.w400,
                                      fontSize:14,
                                      color:const Color.fromRGBO(0,0,0,1),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap:() {
                              
                            },
                            child: Column(
                              children: [
                                Container(
                                  height: 36,
                                  width: 36,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: const Color.fromRGBO(25, 0, 56, 1)
                                  ),
                                  child: Image.asset(
                                    "assets/images/traced2.png"
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Text("VISUAL",
                                    style: GoogleFonts.jost(
                                      fontWeight:FontWeight.w400,
                                      fontSize:14,
                                      color:const Color.fromRGBO(0,0,0,1),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap:() {
                              
                            },
                            child: Column(
                              children: [
                                Container(
                                  height: 36,
                                  width: 36,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: const Color.fromRGBO(25, 0, 56, 1)
                                  ),
                                  child: Image.asset(
                                    "assets/images/traced3.png"
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Text("ILLUSTRATON",
                                    style: GoogleFonts.jost(
                                      fontWeight:FontWeight.w400,
                                      fontSize:14,
                                      color:const Color.fromRGBO(0,0,0,1),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap:() {
                              
                            },
                            child: Column(
                              children: [
                                Container(
                                  height: 36,
                                  width: 36,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: const Color.fromRGBO(25, 0, 56, 1)
                                  ),
                                  child: Image.asset(
                                    "assets/images/traced4.png"
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Text("PHOTO",
                                    style: GoogleFonts.jost(
                                      fontWeight:FontWeight.w400,
                                      fontSize:14,
                                      color:const Color.fromRGBO(0,0,0,1),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ) ,
    );
  }
}