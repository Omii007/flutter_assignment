

import 'package:flutter/material.dart';

class Assignment3 extends StatefulWidget {
  const Assignment3({super.key});

  @override
  State<Assignment3> createState() => _Assignment3State();
}

class _Assignment3State extends State<Assignment3> {

  // variable
  int? selectIndex = 0;

  //List of Images
  final List<String> imageList = [
    "https://media.gettyimages.com/id/1420743616/photo/dubai-united-arab-emirates-players-of-india-line-up-for-the-anthem-during-the-dp-world-asia.jpg?s=612x612&w=0&k=20&c=UAoufmK_Lo-Rx9wKO0S4211UzQnhYwI8WJUmBK3ClxI=",
    "https://media.gettyimages.com/id/1439441491/photo/melbourne-australia-virat-kohli-congratulates-hardik-pandya-of-india-for-getting-the-wicket.jpg?s=612x612&w=0&k=20&c=ZwoxB1GFcupUBx91H5FylaOygzyPRA6IF3waJERhaX8=",
    "https://wallpapercave.com/dwp1x/wp9986475.jpg",
    "https://wallpapercave.com/wp/wp4051785.jpg",
    "https://i.pinimg.com/1200x/4a/a6/5e/4aa65ed3a0700736b0a5c1fdc84d624f.jpg",
    "https://wallpapercave.com/dwp1x/wp3978057.jpg",
    "https://w0.peakpx.com/wallpaper/253/858/HD-wallpaper-virat-kohli-best-india-virat-virat-kohli-best-virat-india-kholi-cricketer.jpg",
    "https://w0.peakpx.com/wallpaper/233/967/HD-wallpaper-virat-kohli-angry-look-king-kohli-indian-cricketer.jpg",
    "https://wallpapercave.com/dwp1x/wp9986442.jpg",
  ];

  void showNextImage(){
    setState(() {
      if(selectIndex ==  imageList.length-1){
        selectIndex = 0;
      }
        selectIndex = selectIndex! + 1;
    });
    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Display Images"),
      ),
      backgroundColor: Color.fromARGB(255, 124, 216, 187),
    body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.network(
            imageList[selectIndex!],
            width: 500,
            height: 500,
          ),
          // const SizedBox(
          //   height: 0,
          // ),
          ElevatedButton(
            onPressed: showNextImage,
            child: const Text("Next",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          ElevatedButton(
            onPressed: (){
              setState(() {
                selectIndex = 0;
              });
            }, child: const Text("Reset",
                  style: TextStyle(
                    color: Colors.black,
                  ),
            ),
          ),
          
        ],
      ),
    ),
    );
  }
}