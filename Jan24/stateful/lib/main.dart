import 'package:flutter/material.dart';
import 'package:stateful/Assignment2/assignment2.dart';
import 'package:stateful/Assignment1/assignment1.dart';
import 'package:stateful/Assignment3/assignment3.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Assignment2(),
    );
  }
}
