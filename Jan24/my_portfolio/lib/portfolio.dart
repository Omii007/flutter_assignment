
import 'package:flutter/material.dart';

class Portfolio extends StatefulWidget {
  const Portfolio({super.key});

  @override
  State<Portfolio> createState() => _PortfolioState();
}

class _PortfolioState extends State<Portfolio> {

  final _name = "Omkar";
  final _collageName = "TSSM'S BSCOER Narhe";
  final _companyName = "Accenture";
  int _counter = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Portfolio",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 25,
          fontStyle: FontStyle.italic,
          color: Colors.black,
          ),
        ),
        backgroundColor: Colors.pink,
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              const Padding(padding: EdgeInsets.all(8.0),),
              (_counter >= 1)?
              Text("Name : $_name",
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                ),
              )
              :const Text(""),
              const Padding(padding: EdgeInsets.all(8.0),),
              (_counter >= 2)?
              SizedBox(
                height: 200,
                width: 200,
                child: Image.asset(
                  "assets/images/SPC_0744 copy.jpg",
                  height: 200,
                  width: 200,
                ),
              )
              : const SizedBox(),
              const Padding(padding: EdgeInsets.all(20.0),),
              (_counter >= 3)?
              Text("College : $_collageName",
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                ),
              )
              : const Text(""),
              const Padding(padding: EdgeInsets.all(8.0),),
              (_counter >= 4)?
              SizedBox(
                height: 200,
                width: 200,
                child: Image.asset(
                  "assets/images/college logo.jpg",
                  height: 200,
                  width: 200,
                ),
              )
              : const SizedBox(),
              const Padding(padding: EdgeInsets.all(20.0),),
              (_counter >= 5)?
              Text("Dream Company : $_companyName",
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                ),
              )
              : const Text(""),
              const Padding(padding: EdgeInsets.all(8.0),),
              (_counter >= 6)?
              SizedBox(
                height: 200,
                width: 200,
                child: Image.asset(
                  "assets/images/accenture logo.png",
                  height: 200,
                  width: 200,
                ),
              )
              : const SizedBox(),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed:(){
          setState(() {
            _counter++;
          });
        },
        child: const Text("Next",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          ),
        ), 
      ),
      persistentFooterButtons: [FloatingActionButton(
        onPressed: (){
          setState(() {
            _counter--;
          });
        },
        child: const Text("Prev",
        style: TextStyle(
          fontWeight: FontWeight.bold,
        ),),
        ),
      ],
    );
  }
}