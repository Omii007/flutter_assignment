import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: SeparatorApp(),
      debugShowCheckedModeBanner:false,
    );
  }
}

class SeparatorApp extends StatefulWidget {
  const SeparatorApp({super.key});

  @override
  State<SeparatorApp> createState()=> _SeparatorAppState();
}

class _SeparatorAppState extends State<SeparatorApp> {

  List<Map> cricketPlayer = [
    {
      "image":"https://akm-img-a-in.tosshub.com/indiatoday/images/media_bank/202308/rohitsharmat20icricketjpg-014825-16x9_0.jpg?VersionId=NYcYBN9i5AxA3nbeXDaJ0o3uWjpGovC0&size=690:388",
      "name":"Rohit",
    },
    {
      "image":"https://timesofindia.indiatimes.com/photo/104569839/104569839.jpg",
      "name": "Virat",
    },
    {  
      "image":"https://w0.peakpx.com/wallpaper/545/93/HD-wallpaper-kl-rahul-india-cricket.jpg",
      "name": "KL Rahul",
    },
    {
      "image": "https://c.ndtvimg.com/2023-03/6doduhno_shubman-gill_625x300_11_March_23.jpg?im=FaceCrop,algorithm=dnn,width=806,height=605",
      "name": "Shubhman",
    },
    {
      "image": "https://pbs.twimg.com/media/F07JykHagAcnLXO.jpg:large",
      "name": "YJ",
    },
    {
      "image": "https://images.indianexpress.com/2021/03/skysports-rishabh-pant-india_5293755.jpg",
      "name": "Rishab",
    },
    {
      "image": "https://wallpapers.com/images/featured/jadeja-pictures-9idqtlrdq16fbokn.jpg",
      "name": "Jadeja",
    },
    {
      "image": "https://www.livemint.com/lm-img/img/2023/09/17/1600x900/TOPSHOT-CRICKET-ASIA-2023-SRI-IND-ODI-2_1694957987850_1694958605789.jpg",
      "name": "Siraj",
    },
    {
      "image":"https://akm-img-a-in.tosshub.com/indiatoday/images/media_bank/202309/hardik-pandya-scored-half-century-vs-pakistan-in-2023-asia-cup-ap-032117-1x1_0.jpg?VersionId=Rlwn5fiXlJPu2SzGgZJYPDm4Y6Al20IF",
       "name": "Hardik",
    },
  ];
  
  // Text separated(int index){

  //   if(index +1 % 3 == 0){
  //     print(index);
  //     return const Text("---------------");
  //   }else{
  //     return Text("");
  //   }
  // }

  @override
  Widget build(BuildContext context){
    int count = 0;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Separator App",
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.orange,
      ),
      body: ListView.separated(
        itemCount: cricketPlayer.length,
        separatorBuilder: (BuildContext context,int index){
          // count++;
          // return SizedBox(
            
          //   child: (count % 3 == 0)? const Center(child:  Text("-------",
          //     style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),
          //   )):const Text(""),
          // );
          if((index + 1) % 3 == 0){
            return const Center(child: Text("------------",
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
              ),
            ));
          }else{
            return const SizedBox();
          }
                    
        },
        itemBuilder: (BuildContext context,int index){
          return Container(
            height: 200,
            width: 200,
            margin: const EdgeInsets.all(10),
            child: Image.network(
              cricketPlayer[index]["image"],
            ),
          );
        }
      
      ),
    );
  }
}
