
import 'package:flutter/material.dart';
import 'package:inheritedwidget_assign/playerdata.dart';

class Player extends InheritedWidget {

  final PlayerDetails playerDetails;

  const Player({
    super.key,
    required this.playerDetails,
    required super.child,
  });

  @override
  bool updateShouldNotify(Player oldWidget){
    return playerDetails.name != oldWidget.playerDetails.name ||
      playerDetails.country != oldWidget.playerDetails.country ||
      playerDetails.team != oldWidget.playerDetails.team
    ;
  }

  static Player of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<Player>()!;
  }
}