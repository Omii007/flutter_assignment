
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:inheritedwidget_assign/inherited_screen.dart';
import 'package:inheritedwidget_assign/playerinfo_screen.dart';

class PlayerData extends StatefulWidget {
  const PlayerData({super.key});

  @override
  State createState() => _PlayerDataState();
}

class PlayerDetails {
  String? name;
  String? country;
  String? team;

  PlayerDetails({
    this.name,
    this.country,
    this.team,
  });
}

class _PlayerDataState extends State {

  TextEditingController nameController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController teamController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Player playerObj = Player.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text("Player Data",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25,
            color: Colors.black,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
        
            const SizedBox(
              height: 20,
            ),
            /// NAME TEXTFORMFIELD
            TextFormField(
              controller: nameController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
                hintText: "Enter player name",
                hintStyle: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                  color: Color.fromRGBO(0,0,0,0.7),
                ),
              ),
            ),
        
            const SizedBox(
              height: 20,
            ),
        
            /// COUNTRY TEXTFORMFIELD
            TextFormField(
              controller: countryController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
                hintText: "Enter player country",
                hintStyle: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                  color: Color.fromRGBO(0,0,0,0.7),
                ),
              ),
            ),
        
            const SizedBox(
              height: 20,
            ),
        
            /// TEAM TEXTFORMFIELD
            TextFormField(
              controller: teamController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
                hintText: "Enter player team",
                hintStyle: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                  color: Color.fromRGBO(0,0,0,0.7),
                ),
              ),
            ),
        
            const SizedBox(
              height: 50,
            ),
        
            GestureDetector(
              onTap: () {
                playerObj.playerDetails.name = nameController.text;
                playerObj.playerDetails.country = countryController.text;
                playerObj.playerDetails.team = teamController.text;

                Navigator.push(
                  context, 
                  MaterialPageRoute(
                    builder: (context)=> const PlayerInfo()));
              },
              child: Container(
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.blue,
                ),
                child: const Center(
                  child: Text("Submit",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}