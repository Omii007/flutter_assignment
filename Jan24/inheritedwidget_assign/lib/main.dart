import 'package:flutter/material.dart';
import 'package:inheritedwidget_assign/inherited_screen.dart';
import 'package:inheritedwidget_assign/playerdata.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return Player(
      playerDetails: PlayerDetails(),
      child: const MaterialApp(
        home: PlayerData(),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
