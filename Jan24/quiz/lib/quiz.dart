
import 'package:flutter/material.dart';

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State<QuizApp> createState()=> _QuizAppState();
}

class _QuizAppState extends State<QuizApp> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Tech Quiz",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.black,
          fontSize: 25, 
          ),
        ),
        backgroundColor: Colors.orange,
      ),
      body: Column(
        children: [
          const Padding(padding: EdgeInsets.all(20),),
          const Text("Question : 1/10",
          style: TextStyle(
            fontSize: 15,
            fontStyle: FontStyle.normal,
            ),
          ),
          const Padding(padding: EdgeInsets.all(15),),
          const Text("Question : What is Flutter?",
          style: TextStyle(
            fontSize: 20,
            fontStyle: FontStyle.normal,
            ),
          ),
          const Padding(
            padding: EdgeInsets.all(15),
          ),
          Container(
            margin: const EdgeInsets.only(left:500,right: 500),
            height: 50,
            width: double.infinity,
            //color: Colors.green,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue,
              ),
              onPressed: (){}, 
              child: const Text(
                "Option 1",
                style: TextStyle(
                  fontSize: 20,
                  
                ),
                
              ),
            ),
          ),
          const Padding(padding: EdgeInsets.all(15),),
          Container(
            margin: const EdgeInsets.only(left:500,right: 500),
            height: 50,
            width: double.infinity,
            color: Colors.blue,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue,
              ),
              onPressed: (){}, 
              child: const Text(
                "Option 2",
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
          ),
          const Padding(padding: EdgeInsets.all(15),),
          Container(
            margin: const EdgeInsets.only(left:500,right: 500),
            height: 50,
            width: double.infinity,
            color: Colors.blue,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue,
              ),
              onPressed: (){}, 
              child: const Text(
                "Option 3",
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
          ),
          const Padding(padding: EdgeInsets.all(15),),
          Container(
            margin: const EdgeInsets.only(left:500,right: 500),
            height: 50,
            width: double.infinity,
            color: Colors.blue,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue,
              ),
              onPressed: (){}, 
              child: const Text(
                "Option 4",
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.navigate_next_outlined),),
    );
  }
}