
import 'package:flutter/material.dart';

class PaddingMargin extends StatelessWidget {
  const PaddingMargin({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Padding & Margin",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 25,
          color: Colors.black,
          ),
        ),
        backgroundColor: Colors.blue.shade300,
      ),
      body: Center(
        child: Container(
          color: Colors.blue,
          child: Container(
            height: 250,
            width: 250,
            color: Colors.amber,
            alignment: Alignment.center,
            padding: const EdgeInsets.all(20),
            margin: const EdgeInsets.all(30),
            child: Image.network(
              "https://cdn.pixabay.com/photo/2017/04/09/09/56/avenue-2215317_1280.jpg",
              height: 250,
              width: 250,
            ),
          ),
        ),
      ),
    );
  }
}