import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    log("IN BUILD MY APP");
    return ChangeNotifierProvider(
      create: (context){
        return Company(
          compName: "Google",
          empCount: 250
        );
      },
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: EmpData(),
      ),
    );
  }
}

class EmpData extends StatefulWidget {
  const EmpData({super.key});

  @override
  State createState()=> _EmpDataState();
}

class _EmpDataState extends State {

  @override
  Widget build(BuildContext context) {
    log("IN EMPDATA BUILD");
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text("ChangeNotifierProvider",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25,
            color:Colors.black,
          ),
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(25.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(Provider.of<Company>(context).compName,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                  color:Colors.black,
                ),
              ),
          
              const SizedBox(
                height: 30,
              ),
              Text("${Provider.of<Company>(context).empCount}",
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                  color:Colors.black,
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              GestureDetector(
                onTap: () {
                  Provider.of<Company>(
                    context,
                    listen: false).changeCompany("Facebook",500);
                },
                child: Container(
                  height: 50,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.blue,
                  ),
                  child: const Center(
                    child: Text("Refresh Data",
                      style:  TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                        color:Colors.black,
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              const NormalClass(),
            ],
          ),
        ),
      ),
    );
  }
}

class NormalClass extends StatelessWidget {
  const NormalClass({super.key});

  @override
  Widget build(BuildContext context) {
    log("IN NORMAL CLASS BUILD");
    return Text(Provider.of<Company>(context).compName,
      style: const TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 20,
        color: Colors.black
      ),
    );
  }
}

class Company extends ChangeNotifier{
  String compName;
  int empCount;

  Company({
    required this.compName,
    required this.empCount
  });
  
  void changeCompany(String compName,int empCount){
    this.compName = compName;
    this.empCount = empCount;
    notifyListeners();
  }
}