import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ToDoApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}
class ToDoApp extends StatefulWidget {
  const ToDoApp({super.key});

  @override
  State createState()=> _ToDoAppState();
}

class _ToDoAppState extends State {

  List cardList = [];
  List cardColor = [
    const Color.fromRGBO(250, 232, 232, 1),
    const Color.fromRGBO(232, 237, 250, 1),
    const Color.fromRGBO(250, 249, 232, 1),
    const Color.fromRGBO(250, 232, 250, 1),

  ];
  int count = -1;

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title:  Text("To-do list",
          style: GoogleFonts.quicksand(
            fontWeight: FontWeight.w600,
            fontSize: 26,
            color: const Color.fromRGBO(255, 255, 255, 1),
          ),
        ),
        backgroundColor: const Color.fromRGBO(2, 167, 177, 1),
      ),
      body: ListView.builder(
          itemCount: cardList.length,
          itemBuilder: (BuildContext context, int index){
            return Padding(
              padding: const EdgeInsets.all(15),
              child: Container(
                height: 112,
                width: 330,
                decoration:  BoxDecoration(
                  color: cardColor[index % cardColor.length],
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  boxShadow:const [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.1),
                      offset: Offset(0, 10),
                      blurRadius: 20,
                      spreadRadius: 1,
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Row(
                      // mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 52,
                          width: 52,
                          margin: const EdgeInsets.only(left: 25),
                          decoration: const BoxDecoration(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            shape: BoxShape.circle,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Image.asset(
                              "assets/images/photo.jpeg",
                              height: 5,
                              width: 6,
                            ),
                          ),
                        ),
                        
                     
                    Column(
                      children: [
                        Container(
                            height: 15,
                            width: 243,
                            margin: const EdgeInsets.only(left: 30,top: 10),
                            child:  Text("Lorem Ipsum is simply setting industry.",
                              style: GoogleFonts.quicksand(
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                                color: const Color.fromRGBO(0, 0, 0, 1),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          Container(
                            height: 44,
                            width: 243,
                            margin: const EdgeInsets.only(left: 30),
                            child: Text("Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
                              style: GoogleFonts.quicksand(
                                fontWeight: FontWeight.w500,
                                fontSize: 10,
                                color: const Color.fromRGBO(84, 84, 84, 1),
                              ),
                            ),
                          ),
                          
                      ],
                    ),
                      ],
                    ),
                    
                    Row(
                          children: [
                            Container(
                              height: 13,
                              width: 70,
                              margin: const EdgeInsets.only(left: 25),
                              child: Text("25 Feb 2024",
                                style: GoogleFonts.quicksand(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 10,
                                  color: const Color.fromRGBO(132, 132, 132, 1),
                                ),
                              ),
                            ),
                            const Spacer(),
                            Container(
                              margin: const EdgeInsets.only(right: 25),
                              height: 8,
                              width: 8,
                              child: IconButton(
                                onPressed: (){}, 
                                icon: const Icon(Icons.edit_outlined,size: 13,)),
                            ),
                            Container(
                              margin: const EdgeInsets.only(right: 25),
                              height: 8,
                              width: 8,
                              child: IconButton(
                                onPressed: (){}, 
                                icon: const Icon(Icons.delete_outlined,size: 13,)),
                            ),
                          ],
                        ),
                  ],
                ),
                
              ),
            );
          },
        ),
      
      floatingActionButton: FloatingActionButton(
        onPressed:(){
          setState(() {
            count++;
            cardList.add(count);
          });
        },
        child: const Icon(Icons.add,size: 40,), ),
    );
  }
}
