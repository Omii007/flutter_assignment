import 'package:flutter/material.dart';

import '../model/product_model.dart';

class ProductController extends ChangeNotifier {
  List<ProductModel> listOfProduct = [];

  /// ADD PRODUCT DATA
  void addProductData({required ProductModel pObj}) {
    listOfProduct.add(pObj);
  }

  /// ADD TO FAVORITE
  void addToFavorite({required int index}) {
    //int index = listOfProduct.indexWhere((element) => element == productModelObj);
    listOfProduct[index].isFavorite = !listOfProduct[index].isFavorite;
    notifyListeners();
  }

  /// ADD QUANTITY
  void addQuantity(int index) {
    listOfProduct[index].quantity = listOfProduct[index].quantity + 1;
    notifyListeners();
  }

  /// REMOVE QUANTITY
  void removeQuantity(int index) {
    listOfProduct[index].quantity = listOfProduct[index].quantity - 1;
    notifyListeners();
  }
}
