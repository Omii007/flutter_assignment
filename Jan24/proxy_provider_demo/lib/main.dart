import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    log("IN MAIN APP BUILD");
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<Login>(
          create: (context) {
            log("IN CHANGE NOTIFIER PROVIDER");
            return Login(
              userName: "Omii007",
              password: "Omii@007",
            );
          },
        ),
        ChangeNotifierProxyProvider<Login, Employee>(create: (context) {
          log("IN PROXY PROVIDER");
          return Employee(
            empId: 8,
            empName: "Omkar",
            userName: Provider.of<Login>(context, listen: false).userName,
            password: Provider.of<Login>(context, listen: false).password,
          );
        }, update: (context, login, employee) {
          return Employee(
            empId: 1,
            empName: "Shiv",
            userName: Provider.of<Login>(context).userName,
            password: Provider.of<Login>(context).password,
          );
        }),
      ],
      child: const MaterialApp(
        home: LoginApp(),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}

class Login with ChangeNotifier {
  String userName;
  String password;

  Login({
    required this.userName,
    required this.password,
  });

  void changePassword(String password, String userName) {
    this.password = password;
    this.userName = userName;
    notifyListeners();
  }
}

class Employee with ChangeNotifier {
  final String empName;
  final int empId;
  final String userName;
  final String password;

  Employee(
      {required this.empId,
      required this.empName,
      required this.userName,
      required this.password});
}

class LoginApp extends StatefulWidget {
  const LoginApp({super.key});

  @override
  State<LoginApp> createState() => _LoginAppState();
}

class _LoginAppState extends State<LoginApp> {
  @override
  Widget build(BuildContext context) {
    log("IN LOGINAPP BUILD");
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Proxy Provider"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Consumer(
              builder: (context, login, child) {
                log("IN CONSUMER");
                return Text(Provider.of<Login>(context).userName);
              },
            ),
            const SizedBox(
              height: 20,
            ),
            Text(Provider.of<Login>(context).password),
            const SizedBox(
              height: 20,
            ),
            Text(Provider.of<Employee>(context).empName),
            const SizedBox(
              height: 20,
            ),
            Text('${Provider.of<Employee>(context).empId}'),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () {
                Provider.of<Login>(context, listen: false)
                    .changePassword("Shiv@24", "Shiv24");
              },
              child: const Text("Change"),
            ),
          ],
        ),
      ),
    );
  }
}
