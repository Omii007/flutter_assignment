

import 'package:flutter/material.dart';

class NetFlixApp extends StatefulWidget {
  const NetFlixApp({super.key});

  @override
  State<NetFlixApp> createState() => _NetFlixAppState();
}

class _NetFlixAppState extends State<NetFlixApp> {

  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "NETFLIX",
          style: TextStyle(
            fontStyle: FontStyle.normal,
            fontSize: 25,
            color: Colors.red,
          ),
        ),
        backgroundColor: Colors.black,
        actions: [
          IconButton(
            onPressed: (){}, 
            icon: const Icon(
              Icons.search,
            ),
          ),
          IconButton(
            onPressed: (){}, 
            icon: const Icon(
              Icons.person,
            ),
          ),
        ],
      ),
      backgroundColor: Colors.black,
      bottomNavigationBar: BottomNavigationBar(

        items: const [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              color: Colors.black,  
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.movie,
              color: Colors.black,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.download,
              color: Colors.black,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.watch_later,
              color: Colors.black,
            ),
            label: '',
          ),
        ],
        backgroundColor: Colors.black,
      ),
      body:SingleChildScrollView(
        
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          
          children: [
            const SizedBox(
              height: 50,
            ),
            const Text("Movies",
            style: TextStyle(
              fontSize: 25,
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.bold,
              color: Colors.white,
              ),
            ),
            
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  Image.network(
                    "https://images.news18.com/ibnkhabar/uploads/2023/09/379075677_699607318195002_4862547145698902485_n-1.jpg",
                    width: 300,
                    height: 350,
                  ),
                  const SizedBox(
                    width: 0,
                  ),
                  Image.network(
                    "https://i.pinimg.com/originals/f6/35/fd/f635fda7eac6e9315006ecfba15db2b6.jpg",
                    width: 300,
                    height: 350,
                  ),
                  Image.network(
                    "https://hombalefilms.com/wp-content/uploads/2021/07/Masterpiece-movie-vertical-banner.jpg",
                    width: 300,
                    height: 350,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 50,
            ),
            const Text("Web Series",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 25,
                color: Colors.white,
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  const SizedBox(
                    width: 5,
                  ),
                  Image.network(
                    "https://i.ytimg.com/vi/_e9y729xeck/maxresdefault.jpg",
                    width: 300,
                    height: 175,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Image.network(
                    "https://img1.hotstarext.com/image/upload/f_auto,t_web_hs_2_5x/sources/r1/cms/prod/4294/754294-h",
                    width: 300,
                    height: 175,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Image.network(
                    "https://blog.shortfundly.com/wp-content/uploads/2021/06/aarya.jpg",
                    width: 300,
                    height: 175,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Image.network(
                    "https://img1.hotstarext.com/image/upload/f_auto,t_web_m_1x/sources/r1/cms/prod/6617/1636617-h-0a45f068df42",
                    width: 300,
                    height: 175,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Image.network(
                    "https://img1.hotstarext.com/image/upload/f_auto,t_web_hs_2_5x/sources/r1/cms/prod/1090/1611090-h-c1c086bd6f3c",
                    width: 300,
                    height: 175,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            const Text("Most Popular",
              style: TextStyle(
                fontSize: 25,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  const SizedBox(
                    width: 5,
                  ),
                  Image.network(
                    "https://assets-in.bmscdn.com/discovery-catalog/events/et00025234-hkxnfqlftf-landscape.jpg",
                    width: 300,
                    height: 150,
                  ),
                  Image.network(
                    "https://www.koimoi.com/wp-content/new-galleries/2023/01/taaza-khabar-review-001.jpg",
                    width: 300,
                    height: 150,
                  ),
                  Image.network(
                    "https://occ-0-2794-2219.1.nflxso.net/dnm/api/v6/6gmvu2hxdfnQ55LZZjyzYR4kzGk/AAAABUjAUS4RuVJx9c6-luFEb8h5DOK85M0_dG6hKr2OYrm3Fdfj_QhL6RW6yDtjPP8NhbSj8sEScOaNaUVRyKm5mmz0f3UdYbExdVC7.jpg?r=41d",
                    width: 300,
                    height: 150,
                  ),
                  Image.network(
                    "https://i.ytimg.com/vi/6CjO0S7tviY/maxresdefault.jpg",
                    width: 300,
                    height: 150,
                  ),
                  Image.network(
                    "https://www.iffigoa.org/public/assets/images/international-cinema-images/60cb558c40e4f18479664069d9642d5a.jpg",
                    width: 300,
                    height: 150,
                  ),
                  Image.network(
                    "https://v3img.voot.com/resizeMedium,w_1090,h_613/v3Storage/assets/bloody-daddy-16x9-1687775864575.jpg",
                    width: 300,
                    height: 150,
                  ),
                ],
              ),
            ),
          ],
          
        ),
      ),
    );
  }
}