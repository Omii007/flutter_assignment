# localdatabse_way2

A new Flutter project.

- In this code insert the data in local databse and after fetch the data in database, this time we can return List of Map.
- then this list of map store in "List" and using for loop access object one by one through index into the list.
- insertData, deleteData, updateData this operation can perform to pass object.
- only fetch or retrive the data in database this time can't pass the object.
- for insertData 3 parameters required
    1. Table name for insert data
    2. Map pass for store data in 'key , value' pair
    3. conflictalgorithm for unique data add or duplicate entries add, they can solve out.
- for updateData 4 parameters required
    1. Table name for update data
    2. Map is required
    3. where: "PRIMARY KEY = ?"
    4. whereArgs: [object.PRIMARY KEY]
- for deleteData 3 parameters required
    1. Table name for delete data
    2. where: "PRIMARY KEY = ?"
    3. whereArgs: [object.PRIMARY KEY]