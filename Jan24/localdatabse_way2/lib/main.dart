import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

dynamic database;

class Employee{
  final String name;
  final int empId;
  final double sal;

  Employee({
    required this.name,
    required this.empId,
    required this.sal,
  });

  Map<String,dynamic> employeeMap(){
    return {
      'name': name,
      'empId': empId,
      'sal': sal,
    };
  }
  
  @override
  String toString(){
    return '{name: $name, empId: $empId, sal: $sal}';
  }
}

// FUNCTION FOR INSERTING DATA IN DATABASE
Future<void> insertEmployeeData(Employee obj)async{
  final localDB = await database;

  await localDB.insert(
    "Employee",
    obj.employeeMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

// FETCH OR RETRIVE DATA IN DATABASE
Future<List<Map<String,dynamic>>> getEmployeeData()async{
  final localDB = await database;

  List<Map<String,dynamic>> mapEntry = await localDB.query("Employee");

  return mapEntry;
}

// DELETE DATA IN DATABASE
Future<void> deleteEmpData(Employee obj)async{
  final localDB = await database;

  await localDB.delete(
    "Employee",
    where: "empId = ?",
    whereArgs : [obj.empId],
  );
}

// UPDATE DATA IN DATABASE
Future<void> updateData(Employee obj)async{
  final localDb = await database;

  localDb.update(
    "Employee",
    obj.employeeMap(),
    where: "empId = ?",
    whereArgs: [obj.empId],

  );
}

void main() async{
  runApp(const MainApp());

  database = openDatabase(
    join(await getDatabasesPath(),"employee.db"),
    version: 1,
    onCreate: (db, version) {
      db.execute(
        '''
          CREATE TABLE Employee(
            name TEXT,
            empId INT PRIMARY KEY,
            sal REAL
          )
        '''
      );
    },
  );

  Employee obj1 = Employee(
    name: "Yash", 
    empId: 1, 
    sal: 1.5,
  );
  Employee obj2 = Employee(
    name: "Prathmesh", 
    empId: 11, 
    sal: 18,
  );

  // for INSERT DATA IN THE DATABASE
  await insertEmployeeData(obj1);
  await insertEmployeeData(obj2);
  
  // FETCH OR RETRIVE THE DATA IN DATABASE
  List<Map<String,dynamic>> retVal = await getEmployeeData();

  // PRINT LIST OF OBJECT ONE BY ONE USING FOR LOOP
  for(int i=0;i<retVal.length;i++){
    print(retVal[i]);
  }

  // DELETE OBJECT IN DATABASE
  await deleteEmpData(obj1);
  print("After delete data in database");

  retVal = await getEmployeeData();
  for(int i=0;i<retVal.length;i++){
    print(retVal[i]);
  }

  Employee obj3 = Employee(
    name: "Omkar", 
    empId: 18, 
    sal: 2.0,
  );

  // ADD DATA IN DATABASE
  await insertEmployeeData(obj3);
  print("After insert data");

  // FETCH OR RETRIVE DATA IN DATABASE
  retVal = await getEmployeeData();
  for(int i=0;i<retVal.length;i++){
    print(retVal[i]);
  }

  // ADD DATA IN DATABASE
  Employee obj4 = Employee(
    name: "Abhi", 
    empId: 18, 
    sal: 2.0,
  );

  await insertEmployeeData(obj4);
  print("After insert data");

  // FETCH OR RETRIVE DATA IN DATABASE
  retVal = await getEmployeeData();
  for(int i=0;i<retVal.length;i++){
    print(retVal[i]);
  }

  // UPDATE DATA IN DATABASE
  obj4 = Employee(
    name: "Omii", 
    empId: 18, 
    sal: 25.0,
  );

  await updateData(obj4);
  print("After update data");

  // AFTER UPDATE DATA IN DATABASE FETCH
  retVal = await getEmployeeData();
  for(int i=0;i<retVal.length;i++){
    print(retVal[i]);
  }
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body: Center(
          child: Text('Hello World!'),
        ),
      ),
    );
  }
}
