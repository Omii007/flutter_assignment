
import 'package:flutter/material.dart';

class Flag extends StatefulWidget {
  const Flag({super.key});

  @override
  State<Flag> createState()=> _FlagState();
}

class _FlagState extends State<Flag> {

  int _counter = 0;

  void counterIncriment(){
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("India Flag",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.black,
          fontSize: 25,
          ),
        ),
        backgroundColor: Colors.orange,
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              children: [
                const Padding(padding: EdgeInsets.only(top: 208),),
                (_counter >= 1)?
                Container(
                  height: 400,
                  width: 3,
                  color: Colors.black,
                )
                : Container(),
              ],
            ),
            Column(
              children: [
                const Padding(padding: EdgeInsets.only(top: 208),),
                (_counter >= 2)?
                Container(
                  height: 70,
                  width: 400,
                  color: Colors.orange,
                )
                : Container(),
                (_counter >= 3)?
                Container(
                  height: 70,
                  width: 400,
                  color: Colors.white,
                  child: (_counter >= 4)?
                  Image.network(
                    "https://media.istockphoto.com/id/1182479181/vector/ashoka-chakra.jpg?s=612x612&w=0&k=20&c=bDrv1c_tw5pOa-R0Agg62ZjeMJl3S22Trk-ViEhJiIU=",
                    height: 70,
                    width: 70,
                  )
                  :Container(),
                )
                : Container(),
                (_counter >= 5)?
                Container(
                  height: 70,
                  width: 400,
                  color: Colors.green,
                )
                :Container(),
              ],
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed:counterIncriment,
        child: const Text("Add"),
      ),
    );
  }
}