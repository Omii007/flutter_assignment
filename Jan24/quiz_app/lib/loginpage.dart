
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:quiz_app/main.dart';

class LoginApp extends StatefulWidget{
  const LoginApp({super.key});

  @override
  State createState()=> _LoginAppState();
}

class _LoginAppState extends State {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _flag = true;
  void showPassword(){
    setState(() {
      _flag = !_flag;
    });
  }

  void loginShow(){
    setState(() {
      bool loginValidated = _formKey.currentState!.validate();
            
      if(_usernameController.text.trim().isEmpty || 
        _passwordController.text.trim().isEmpty
      ){
        return;
      }else{
        if(_usernameController.text == "Omii007" && 
        _passwordController.text == "Omkar@007" && loginValidated){
          Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => 
              const Start(),
            ),);
            ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text("Login Successfull"),
            ),
          );
        }else{
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text("Login Failed"),
            ),
          );
        }
      }
    });
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 80),
                  child: Center(
                    child: Image.asset(
                      "assets/images/login.png",
                      height: 200,
                      width: 200,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                TextFormField(
                  controller: _usernameController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                    hintText: "Enter username",
                    label: const Text("Enter username"),
                  ),
                  validator: (value) {
                    if(value == null || value.trim().isEmpty){
                      return "Please enter username";
                    }else{
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,
                ),
                const SizedBox(
                  height: 30,
                ),
                TextFormField(
                  controller: _passwordController,
                  obscureText: _flag,
                  decoration: InputDecoration(
                    suffixIcon: GestureDetector(
                      onTap: () {
                        showPassword();
                      },
                      child: (_flag)?const Icon(Icons.visibility_off) : const Icon(Icons.visibility),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                    hintText: "Enter password",
                    label: const Text("Enter password"),
                  ),
                  validator: (value) {
                    if(value == null || value.trim().isEmpty){
                      return "Please enter password";
                    }else{
                      return null;
                    }
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
                SizedBox(
                  height: 50,
                  width: 300,
                  child: ElevatedButton(
                    onPressed: (){
                      loginShow();
                      
                    }, 
                    style: const ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(Colors.blue),
                      shape: MaterialStatePropertyAll(
                        RoundedRectangleBorder(
                          side: BorderSide(
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                    child: Text("Login",
                      style: GoogleFonts.quicksand(
                        fontSize:25,
                        fontWeight:FontWeight.w700,
                        color:const Color.fromRGBO(255, 255, 255, 1),
                      ),
                    )
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}