// import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:quiz_app/loginpage.dart';
import 'package:quiz_app/quizapp.dart';
// import 'package:http/http.dart' as http;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      home: const LoginApp(),
      debugShowCheckedModeBanner:false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          seedColor: const Color.fromRGBO(250, 232, 250, 1),
          background: const Color.fromRGBO(232, 237, 250, 1),
          primary: Colors.black,
        ),
      ),
    );
  }
}
class Start extends StatefulWidget {
  const Start({super.key});

  @override
  State createState()=> _StartState();
}
class _StartState extends State {

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Quiz App",
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: 25,
            color: Colors.orange
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Column(
         
           mainAxisAlignment: MainAxisAlignment.center,
          children: [
            
            Container(
              height: 300,
              width: 300,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    blurRadius: 20,
                    offset: Offset(10, 10),
                    spreadRadius: 2,
                  ),
                ],
              ),
              child: Image.asset(
                "assets/images/quiz-logo.jpg",
                
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Container(
                height: 50,
                width: 150,
                decoration: const BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black,
                      blurRadius: 40,
                      offset: Offset(10, 10),
                      spreadRadius: 2,
                    ),
                  ],
                ),
                child: ElevatedButton(
                  onPressed: (){
                    setState(() {
                      Navigator.push(
                        context, 
                        MaterialPageRoute(builder: (context)=> const QuizApp()));
                    });
                  }, 
                  child: const Text("Start Quiz",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.black,
                    ),
                  )
                  ),
              ),
          ],
        ),
      ),
    );
  }
}

