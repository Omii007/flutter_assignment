import'package:flutter/material.dart';

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State<QuizApp> createState() => _QuizAppState();
}

class SingleModelClass{
  final String? question;
  final List<String>? options;
  final int? answerIndex;
 
  const SingleModelClass({this.question,this.options,this.answerIndex});
}

class _QuizAppState extends State<QuizApp> {

  List allQuestions = [
    const SingleModelClass(
      question: "Which data type can hold the value of both int and double in dart?",
      options: ["numeric", "int", "double", "num"],
      answerIndex: 3,
    ),
     const SingleModelClass(
      question: "Which of the following data can be used to represent 'John'?",
      options: ["string", "char", "String", "str"],
      answerIndex: 2,
    ),
     const SingleModelClass(
      question: "To declare a variable in Dart, without specifying its data type you can use the keyword _____?",
      options: ["val", "var", "let", "num"],
      answerIndex: 1,
     ),
     const SingleModelClass(
      question: "_____ data type can be used to represent true or false?",
      options: ["Boolean", "bool", "int", "num"],
      answerIndex: 1,
     ),
     const SingleModelClass(
      question: "Which of the following is not an Arithmetic operator?",
      options: ["-", "%", "/", "?"],
      answerIndex: 3,
     ),
     const SingleModelClass(
      question: "While performing bitwise XOR if both the bits being compared have the same value then what must be the outcome?",
      options: ["0", "1", "Can't decide", "100"],
      answerIndex: 0,
     ),
     const SingleModelClass(
      question: "The outcome of the operation performed using the Relational Operator is of type____?",
      options: ["int", "double", "bool", "Will be decided based on operands"],
      answerIndex: 2,
     ),
     const SingleModelClass(
      question: "_____ property is used to check the runtime data type?",
      options: ["runtime", "typeOf", "instanceOf", "runtimeType"],
      answerIndex: 3,
     ),
     const SingleModelClass(
      question: "How do you call a lambda function stored in a variable?",
      options: ["addition()", "addition.call()", "call(addition)", "execute(addition)"],
      answerIndex: 0,
     ),
     const SingleModelClass(
      question: "The ____ part of a 'for' loop defines how the loop control variables should be updated after each iteration?",
      options: ["Initialization", "Condition", "Iteration", "All of the above"],
      answerIndex: 2,
     ),
     
  ];

  bool questionScreen = true;
  int questionIndex = 0;
  bool isButtonPress = false;
  bool isTrue = false;
  int selectedIndex = -1;
  int correctAnswerIndex = -1;
  int correctAnswer = 0;
  
  Color? buttonColor(int selectedIndex){

    if(isTrue && selectedIndex == this.selectedIndex){
      correctAnswer++;
      return Colors.green;
    }else{

      if(selectedIndex == correctAnswerIndex){
        return Colors.green;
      }
      if(!isTrue && this.selectedIndex == selectedIndex){
        return Colors.red;
      }
    }
    return null;
  }

  void functionAnswer(int selectedIndex){
    correctAnswerIndex = allQuestions[questionIndex].answerIndex;
    this.selectedIndex = selectedIndex;

    if(selectedIndex == correctAnswerIndex){
      isTrue = true;
    }else{
      isTrue = false;
    }
  }

  // Future<void> fetchData()async{
  //   final response = await http.get(Uri.parse("https://jsonplaceholder.typicode.com/posts"));

  //   if(response.statusCode == 200){
  //     List<dynamic> data = jsonDecode(response.body);
  //     print(data[0]['id']);
  //   }else{
  //     print("Failed to load data: ${response.statusCode}");
  //   }
  // }

  void floatingFunction(){
    setState(() {
      if(isButtonPress){
        if(questionIndex < allQuestions.length-1){
            questionIndex++;
            correctAnswerIndex = -1;
            isButtonPress = false;
            selectedIndex = -1;
        }else{
          questionIndex = 0;
          selectedIndex = -1;
          isButtonPress = false;
          correctAnswerIndex = -1;
          questionScreen = false;
        }
      }
    });
  }
 
  Scaffold isQuestionScreen(){

    if(questionScreen == true){

      Size screenSize = MediaQuery.sizeOf(context);
      return Scaffold(
        appBar: AppBar(
          title: const Text("QuizApp",
            style: TextStyle(
              fontSize:30,
              fontWeight:FontWeight.w800,
              color: Colors.orange,
            ),
          ),
          centerTitle:true,
          backgroundColor:Colors.blue,
        ),
        body: Column(
          children: [
            const SizedBox(
              height: 25,
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text("Questions : ",
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w600,
                  ),
                ),
                Text("${questionIndex + 1}/${allQuestions.length}",
                  style: const TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 50,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10,right: 10),
              child: Expanded(
                child: Container(
                  padding: const EdgeInsets.all(5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: const Color.fromRGBO(255, 232, 232, 1),
                    boxShadow: const [
                      BoxShadow(
                        color: Color.fromRGBO(234, 135, 96, 1),
                        offset: Offset(0, 5),
                        blurRadius: 10,
                        spreadRadius: 5,
                      ),
                    ],
                  ),
                  child: Text(
                    allQuestions[questionIndex].question,
                    style: const TextStyle(
                      fontSize: 23,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Container(
              height: screenSize.height * 0.06,
              width: screenSize.width* 0.9,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                boxShadow: const [
                  BoxShadow(
                    color: Color.fromRGBO(179, 196, 239, 1),
                    blurRadius: 10,
                    spreadRadius: 5,
                    offset: Offset(0, 5),
                  ),
                ],
              ),
              child: ElevatedButton(
                onPressed: (){
                    if(!isButtonPress){
                    setState(() {
                      isButtonPress = true;
                        functionAnswer(0);
                    });
                  }
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(buttonColor(0),
                  ),
                ),
                child: Text(
                  "A. ${allQuestions[questionIndex].options[0]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                    color: Colors.black
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: screenSize.height * 0.06,
              width: screenSize.width* 0.9,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                boxShadow: const [
                  BoxShadow(
                    color: Color.fromRGBO(179, 196, 239, 1),
                    blurRadius: 10,
                    spreadRadius: 5,
                    offset: Offset(0, 5),
                  ),
                ],
              ),
              child: ElevatedButton(
                onPressed: (){
                  if(!isButtonPress){
                    setState(() {
                      isButtonPress = true;
                      functionAnswer(1);
                    });
                  }
                }, 
                style: ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(buttonColor(1),
                  ),
                ),
                child: Text(
                  "B. ${allQuestions[questionIndex].options[1]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: screenSize.height * 0.06,
              width: screenSize.width* 0.9,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                boxShadow: const [
                  BoxShadow(
                    color: Color.fromRGBO(179, 196, 239, 1),
                    blurRadius: 10,
                    spreadRadius: 5,
                    offset: Offset(0, 5),
                  ),
                ],
              ),
              child: ElevatedButton(
                onPressed: (){
                  if(!isButtonPress){
                    setState(() {
                      isButtonPress =true;
                      functionAnswer(2);
                    });
                  }
                }, 
                style: ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(buttonColor(2),
                  ),
                ),
                child: Text(
                  "C. ${allQuestions[questionIndex].options[2]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                    color: Colors.black
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: screenSize.height * 0.06,
              width: screenSize.width* 0.9,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                boxShadow: const [
                  BoxShadow(
                    color: Color.fromRGBO(179, 196, 239, 1),
                    blurRadius: 10,
                    spreadRadius: 5,
                    offset: Offset(0, 5),
                  ),
                ],
              ),
              child: ElevatedButton(
                onPressed: (){
                  if(!isButtonPress){
                    setState(() {
                      isButtonPress = true;
                      functionAnswer(3);
                    });
                  }
                }, 
                style: ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(buttonColor(3),
                  ),
                ),
                child: Text(
                  "D. ${allQuestions[questionIndex].options[3]}",
                  style:const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                    color: Colors.black,
                  )
                ),
              ),
            ),
          ],
        ),  
        floatingActionButton:FloatingActionButton(
          onPressed: (){
            floatingFunction();
          },
          backgroundColor: Colors.blue,
          child: const Icon(Icons.forward,
            color: Colors.orange,
          ),  
        ),
      );
    }else{
      return Scaffold(
        appBar: AppBar(
          title: const Text("QuizApp",
            style:TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w800,
              color:Colors.orange,
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.blue,
        ),
        backgroundColor: const Color.fromARGB(255, 240, 242, 242),
        body: Column(
          
          children: [
            const SizedBox(
              height: 30,
            ),
            
            Center(
              child: Image.asset(
                "assets/images/trophy.webp",
                width: 350,
              ),
            ),
            const SizedBox(
              height: 30,
            ),

            const Text("You have completed quiz",
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.w600,
                color: Colors.black,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Text("Result : $correctAnswer/${allQuestions.length}",
              style: const TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.normal,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: (){
                setState(() {
                  questionScreen = true;
                  questionIndex = 0;
                  correctAnswer = 0;
                });
              }, 
              style:  ButtonStyle(backgroundColor: MaterialStatePropertyAll(Colors.blue.shade300)),
              child: const Text("Restart",
                style:TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  color: Colors.black
                ),
              ),
              
            ),
          ],
        ),
      );
    }
  }
  
  @override
  Widget build(BuildContext context){

    return isQuestionScreen();
  }
}
