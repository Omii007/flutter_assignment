
import 'package:flutter/material.dart';

class ColorChange extends StatefulWidget {
  const ColorChange({super.key});

  @override
  State<ColorChange> createState() => _ColorChangeState();
}

class _ColorChangeState extends State<ColorChange> {

  int _counter1 = 0;
  int _counter2 = 0;

  Color? colorBox1(){

    if(_counter1 == 0){
      return Colors.blue;
    }else if(_counter1 == 1){
      return Colors.pink;
    }else if(_counter1 == 2){
      return Colors.orange;
    }else if(_counter1 == 3){
      return Colors.green;
    }else{
      _counter1 = 0;
      return Colors.indigo;
    }
  }

  Color? colorBox2(){

    if(_counter2 == 0){
      return Colors.blue;
    }else if(_counter2 == 1){
      return Colors.pink;
    }else if(_counter2 == 2){
      return Colors.orange;
    }else if(_counter2 == 3){
      return Colors.green;
    }else{
      _counter2 = 0;
      return Colors.indigo;
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Color Change",
        style:TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
          fontSize: 25,
          fontStyle: FontStyle.italic,
          ),
        ),
        backgroundColor: Colors.orange,
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 200,
                width: 200,
                color: colorBox1(),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: (){
                  setState(() {
                    _counter1++;
                  });
                }, 
                child: const Text("Switch",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontStyle: FontStyle.normal,
                ),),
              )
            ],
          ),
          const SizedBox(
            width: 30,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 200,
                width: 200,
                color: colorBox2(),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: (){
                  setState(() {
                      _counter2++;
                  });
                }, 
                child: const Text("Switch",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontStyle: FontStyle.normal,
                ),),
              )
            ],
          ),
        ],
      ),
    );
  }
}