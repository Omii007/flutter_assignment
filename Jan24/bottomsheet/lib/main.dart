import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: BottomSheetDemo(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class BottomSheetDemo extends StatefulWidget{
  const BottomSheetDemo({super.key});

  @override
  State createState()=> _BottomSheetDemoState();
}
class _BottomSheetDemoState extends State{

  TextEditingController nameController = TextEditingController();


  @override
  Widget build (BuildContext context){

    return Scaffold(

      appBar: AppBar(
        title: const Text("Bottom Sheet",
        style: TextStyle(fontWeight: FontWeight.bold,
        color: Colors.black),),
        backgroundColor: Colors.blue,
      ),
      
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          showModalBottomSheet(
            context: context, 
            builder: (context) {
              return  Column(
                children: [
                   const Text("To Do List",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  const Text("Title",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                      fontSize: 20,
                    ),
                  ),
                  
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: TextField(
                      controller: nameController,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                      ),
                    ),
                  ),
                  const Text("Description",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                      fontSize: 20,
                    ),
                  ),
                  
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: TextField(
                      controller: nameController,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                      ),
                    ),
                  ),
                  const Text("Date",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                      fontSize: 20,
                    ),
                  ),
                  
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: TextField(
                      controller: nameController,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                      ),
                    ),
                  ),
                  ElevatedButton(
                    onPressed: (){}, 
                    child: const Text("Submit",
                      style: TextStyle(
                        color: Colors.black, 
                      ),
                      
                    ),
                    
                  ),
                  
                ],
              );
            },);
        },
        child: const Text("Add",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.black,
            fontSize: 25,
          ),
        ),
        ),
    );

  }
}
  

