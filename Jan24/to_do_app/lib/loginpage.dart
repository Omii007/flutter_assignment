import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:to_do_app/main.dart';

class LoginApp extends StatefulWidget {
  const LoginApp({super.key});

  @override
  State<StatefulWidget> createState() {
    return _LoginAppState();
  }
}

class _LoginAppState extends State {

  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool _flag = true;

  void showPassword(){
    setState(() {
      _flag = !_flag;
    });
  }

  void showLogin(){
    setState(() {
      bool loginValidated = _formKey.currentState!.validate();

    if(_usernameController.text.trim().isEmpty&&
      _passwordController.text.trim().isEmpty
    ){
      return;
    }else{
      if(_usernameController.text == "Omii007" && 
        _passwordController.text == "Omkar@007" && loginValidated){
          Navigator.of(context).push(
          MaterialPageRoute(builder: (context)=>
            const ToDoApp()
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text("Login Successful"),
          ),
        );
      }else{
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text("Login Failed"),
          ),
        );
      }
    }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(255,232,249,1),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                const SizedBox(
                  height: 40,
                ),
                Center(
                  child: Image.asset(
                    "assets/images/image.webp",
                    height: 200,
                    width: 200,
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
                TextFormField(
                  controller: _usernameController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    hintText: "Enter username",
                    label: const Text("Enter username"),
                  ),
                  keyboardType: TextInputType.emailAddress,
                  validator: (value){
                    if(value == null || value.trim().isEmpty){
                      return "Please enter username";
                    }else{
                      return null;
                    }
                  },
                ),
                const SizedBox(
                  height: 40,
                ),
                TextFormField(
                  controller: _passwordController,
                  obscureText: _flag,
                  decoration: InputDecoration(
                    suffixIcon: GestureDetector(
                      onTap: showPassword,
                      child: 
                      (_flag)? const Icon(Icons.visibility_off) : const Icon(Icons.visibility),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    hintText: "Enter password",
                    label: const Text("Enter password"),
                  ),
                  validator: (value){
                    if(value == null || value.trim().isEmpty){
                      return "Please enter password";
                    }else{
                      return null;
                    }
                  }
                ),
                const SizedBox(
                  height: 40,
                ),
                SizedBox(
                  height: 50,
                  width: 300,
                  child: ElevatedButton(
                    onPressed: (){
                      showLogin();
                    }, 
                    style: const ButtonStyle(
                      shape: MaterialStatePropertyAll(
                        RoundedRectangleBorder(
                          side: BorderSide(color: Color.fromRGBO(0, 0, 0, 1)),
                          borderRadius: BorderRadius.all(Radius.zero),
                        ),
                      ),
                      backgroundColor: MaterialStatePropertyAll(Colors.blue),
                    ),
                  child: Text("Login",
                    style: GoogleFonts.quicksand(
                      fontWeight: FontWeight.w700,
                      fontSize:25,
                      color:const Color.fromRGBO(255, 255, 255, 1),
                    ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}