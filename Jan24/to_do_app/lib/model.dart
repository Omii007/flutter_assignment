
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ToDoApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class ToDoApp extends StatefulWidget {
  const ToDoApp({super.key});

  @override
  State<ToDoApp> createState() => _ToDoAppState();
}

class ToDoModelClass{
  final String title;
  final String description;
  final String date;

  const ToDoModelClass({
    required this.title,
    required this.description,
    required this.date,
  });
}

class _ToDoAppState extends State <ToDoApp> {

  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController dateController = TextEditingController();


  List colorList = 
      [
        const Color.fromRGBO(250, 232, 232, 1),
        const Color.fromRGBO(232, 237, 250, 1),
        const Color.fromRGBO(250, 232, 250, 1),
        const Color.fromRGBO(250, 232, 232, 1),
      ];

  List<ToDoModelClass> toDoList = [];
  int count = -1;
  // bool flag = false;
  // bool isSending = false;

  // for Add a card
  void isBottomSheet({int? editIndex }){
    String title;
    String description;
    String date;

    // Already data filled in controller then if block execute.
    if(editIndex != null){
      titleController.text = toDoList[editIndex].title;
      descriptionController.text = toDoList[editIndex].description;
      dateController.text = toDoList[editIndex].date;
    }
      showModalBottomSheet(
      isScrollControlled: true,
      context: context, 
      builder: (context) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(
                height: 10,
              ),
              Text("Create Task",
                style: GoogleFonts.quicksand(
                  fontWeight: FontWeight.w600,
                  fontSize: 22,
                  color: const Color.fromRGBO(0, 0, 0, 1),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Text("Title",
                      style: GoogleFonts.quicksand(
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        color: const Color.fromRGBO(0, 139, 148, 1),
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    TextField(
                      controller: titleController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        hintText: "Enter a title",
                        hintStyle: GoogleFonts.quicksand(
                          fontWeight: FontWeight.normal,
                          fontSize: 15,
                          color: const Color.fromRGBO(0,0,0,1),
                        ),
                      ),
                      // onSubmitted: (value) {
                      //   if(value.isEmpty || value.trim().length <= 1){
                      //     "Please entered title";
                      //   }
                      // },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Text("Description",
                      style: GoogleFonts.quicksand(
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        color: const Color.fromRGBO(0, 139, 148, 1),
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    TextField(
                      controller: descriptionController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        hintText: "Enter a description",
                        hintStyle: GoogleFonts.quicksand(
                          fontWeight: FontWeight.normal,
                          fontSize: 15,
                          color: const Color.fromRGBO(0,0,0,1),
                        ),
                      ),
                      // onSubmitted: (value) {
                      //   if(value == null || value.isEmpty || value.trim().length <= 1){
                      //     "Please entered description";
                      //   }
                      // },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Text("Date",
                      style: GoogleFonts.quicksand(
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        color: const Color.fromRGBO(0, 139, 148, 1),
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    TextField(
                      controller: dateController,
                      decoration: InputDecoration(
                        suffixIcon: const Icon(Icons.calendar_month_outlined,size: 25,),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        hintText: "Enter a date",
                        hintStyle: GoogleFonts.quicksand(
                          fontWeight: FontWeight.normal,
                          fontSize: 15,
                          color: const Color.fromRGBO(0,0,0,1),
                        ),
                      ),
                      // onSubmitted: (value) {
                      //   if(value == null || value.isEmpty || value.trim().length<= 1){
                      //     'Please entered date';
                      //   }
                        
                      // },
                      readOnly: true,
                      onTap: ()async {
                        // pick the Date from datepicker
                
                        DateTime? pickDate = await showDatePicker(
                          context: context, 
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2024), 
                          lastDate: DateTime(2025),
                
                        );
                        String formatedDate = DateFormat.yMMMd().format(pickDate!);
                
                        setState(() {
                          dateController.text = formatedDate;
                        });
                      },
                      
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                height: 50,
                width: 300,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0),
                  boxShadow: const[
                    BoxShadow(
                      color: Color.fromRGBO(0,0,0,0.1),
                      blurRadius: 10,
                      spreadRadius: 1,
                    ),
                  ],
                ),
                child: ElevatedButton(
                  onPressed: (){
                    
                      setState(() {
                        title = titleController.text;
                        description = descriptionController.text;
                        date = dateController.text;
                      
                      if(titleController.text.trim().isEmpty || titleController.text.isEmpty 
                        && descriptionController.text.trim().isEmpty || descriptionController.text.isEmpty
                        && dateController.text.trim().isEmpty || dateController.text.isEmpty){
                          return;
                        }  
                      if(editIndex != null){
                        toDoList[editIndex] = ToDoModelClass(
                          title: title, 
                          description: description, 
                          date: date,);
                      } else{                   
                      toDoList.add(ToDoModelClass(
                          title: title, 
                          description: description, 
                          date: date,
                        ),
                      );
                      }
                      Navigator.of(context).pop();
                      },
                    );  
                  }, 
                  style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(Color.fromRGBO(0,139,148,1))
                  ),
                  child: Text("Submit",
                    style: GoogleFonts.inter(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      color: const Color.fromRGBO(255,255,255,1),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
            ],
          ),
        );
      },);
  }

  @override
  Widget build(BuildContext context){

    return Scaffold(
      appBar: AppBar(
        title: Text("To-do list",
          style: GoogleFonts.quicksand(
            color: const Color.fromRGBO(255, 255, 255, 1),
            fontWeight: FontWeight.w700,
            fontSize: 26, 
          ),  
        ),
        backgroundColor: const Color.fromRGBO(2, 167, 177, 1),
      ),
      body: ListView.builder(
        itemCount: toDoList.length,
        itemBuilder: (BuildContext context ,int index){
          return Padding(
            padding: const EdgeInsets.only(left: 15,right: 15,top: 20),
            child: Container(
              height: 112,
              width: 330,
              decoration:  BoxDecoration(
                borderRadius:const BorderRadius.all(Radius.circular(10)),
                color: colorList[index%colorList.length],
                boxShadow: const [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.1),
                    spreadRadius: 1,
                    blurRadius: 20,
                    offset: Offset(0, 10),
                  ),
                ],
              ),
              child: Column(
                children: [
                  // Row 1
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(15),
                        child: Column(
                          children: [
                            Container(
                              height: 52,
                              width: 52,
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color.fromRGBO(255, 255, 255, 1),
                                boxShadow: [
                                  BoxShadow(
                                    color: Color.fromRGBO(0, 0, 0, 0.07),
                                    blurRadius: 10,
                                  )
                                ],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image.asset(
                                  "assets/images/photo.jpeg",
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: 10,
                          ),
                          Text(toDoList[index].title,
                            style: GoogleFonts.quicksand(
                                fontWeight: FontWeight.w600,
                                fontSize: 12,
                                color: const Color.fromRGBO(0, 0, 0, 1),
                              ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.06,
                            width: MediaQuery.of(context).size.width * 0.7,
                            child: Text(toDoList[index].description,
                              style: GoogleFonts.quicksand(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 10,
                                  color: const Color.fromRGBO(84, 84, 84, 1),
                                ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  // Row 2
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 15),
                        child: Text(toDoList[index].date,
                          style: GoogleFonts.quicksand(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                    color: const Color.fromRGBO(132, 132, 132, 1),
                                  ),
                        ),
                      ),
                      const Spacer(),
                      Row(
                        children: [
                          GestureDetector(
                            child: const Icon(Icons.edit_outlined,
                              size: 15,
                              color: Color.fromRGBO(0, 139, 148, 1),
                            ),
                            onTap: (){
                              //  flag = false;
                              setState(() {
                                
                                isBottomSheet(editIndex : index);
                              });
                            },
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          GestureDetector(
                            child: const Icon(Icons.delete_outline_outlined,
                              size: 15,
                              color: Color.fromRGBO(0, 139, 148, 1),
                            ),
                            onTap: (){
                              setState(() {
                                toDoList.removeAt(index);
                              });
                              
                            },
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            titleController.clear();
            descriptionController.clear();
            dateController.clear();
            isBottomSheet();
          });
        },
        child: const Icon(Icons.add,
          size: 40,
        ),
      ),
    );
  }
}
