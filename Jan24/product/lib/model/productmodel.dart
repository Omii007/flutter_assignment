class ProductModel {
  String? image;
  String? name;
  String? price;
  bool favourite = false;
  int count = 0;

  ProductModel({
    this.image,
    this.name,
    this.price,
    this.favourite = false,
    this.count = 0,
  });
}
