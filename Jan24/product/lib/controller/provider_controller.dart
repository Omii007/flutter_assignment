import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:product/model/productmodel.dart';

class ProductController with ChangeNotifier {
  ProductModel? productModel;
  List<ProductModel> productModelList = [];
  List<ProductModel> wishList = [];

// object yenar ahe
  /// ADD DATA
  void addData(ProductModel obj) {
    productModelList.add(obj);
    print(productModelList);
    notifyListeners();
  }

  void decrementCount(int index) {
    productModelList[index].count--;
    log('COUNT DECREMENT');
    notifyListeners();
  }

  void incrementCount(int index) {
    productModelList[index].count++;
    log('COUNT INCREMENT');
    notifyListeners();
  }

  void addFavourite({required int index}) {
    productModelList[index].favourite = !productModelList[index].favourite;
    print(productModelList[index].favourite);
    notifyListeners();
  }

  void addWish({ProductModel? obj}) {
    wishList.add(obj!);
    print("ADD successfully $productModelList");
    notifyListeners();
  }

  void removeWish({int? obj, int? index}) {
    wishList.removeAt(obj!);
    notifyListeners();
  }
}
