import 'package:flutter/material.dart';
import 'package:product/controller/provider_controller.dart';
import 'package:product/view/wishlist_scree.dart';
import 'package:provider/provider.dart';

class ProductDetails extends StatefulWidget {
  const ProductDetails({super.key});

  @override
  State createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  @override
  Widget build(BuildContext context) {
    final providerObj = Provider.of<ProductController>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text(
          "Product Details",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
            color: Colors.black,
          ),
        ),
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const WishList(),
                ),
              );
            },
            child: const Icon(
              Icons.favorite_border_rounded,
              color: Colors.red,
            ),
          )
        ],
      ),
      body: ListView.builder(
          itemCount: providerObj.productModelList.length,
          itemBuilder: (BuildContext context, int index) {
            final product = providerObj.productModelList[index];
            return Padding(
              padding: const EdgeInsets.all(24.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 150,
                    width: double.infinity,
                    child: Image.network(
                      product.image.toString(),
                      fit: BoxFit.cover,
                      filterQuality: FilterQuality.high,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "${product.name}",
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      color: Colors.black,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "${product.price}",
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      color: Colors.black,
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Provider.of<ProductController>(context, listen: false)
                              .addFavourite(index: index);
                          if (providerObj.productModelList[index].favourite) {
                            Provider.of<ProductController>(context,
                                    listen: false)
                                .addWish(
                                    obj: providerObj.productModelList[index]);
                          } else {
                            Provider.of<ProductController>(context,
                                    listen: false)
                                .removeWish(obj: index);
                          }
                        },
                        child: Consumer<ProductController>(
                          builder: (context, value, child) {
                            return Icon(
                              product.favourite
                                  ? Icons.favorite_outlined
                                  : Icons.favorite_border_rounded,
                              color: Colors.red,
                            );
                          },
                        ),
                      ),
                      const Spacer(),
                      GestureDetector(
                        onTap: () {
                          Provider.of<ProductController>(context, listen: false)
                              .incrementCount(index);
                        },
                        child: const CircleAvatar(
                          backgroundColor: Colors.blue,
                          radius: 20,
                          child: Icon(
                            Icons.add,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Consumer<ProductController>(
                          builder: (context, value, child) {
                            return Text(
                              "${product.count}",
                              style: const TextStyle(
                                fontWeight: FontWeight.normal,
                                fontSize: 15,
                                color: Colors.black,
                              ),
                            );
                          },
                        ),
                      ),
                      const SizedBox(
                        width: 0,
                      ),
                      GestureDetector(
                        onTap: () {
                          Provider.of<ProductController>(context, listen: false)
                              .decrementCount(index);
                        },
                        child: const CircleAvatar(
                          backgroundColor: Colors.blue,
                          radius: 20,
                          child: Icon(
                            Icons.remove,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            );
          }),
    );
  }
}
