import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:product/controller/provider_controller.dart';
import 'package:provider/provider.dart';

class WishList extends StatefulWidget {
  const WishList({super.key});

  State createState() => _WishListState();
}

class _WishListState extends State {
  @override
  Widget build(BuildContext context) {
    final providerObj = Provider.of<ProductController>(context);
    return Scaffold(
      body: ListView.builder(
          itemCount: providerObj.wishList.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.all(24.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 150,
                    width: double.infinity,
                    child: Image.network(
                      providerObj.wishList[index].image.toString(),
                      fit: BoxFit.cover,
                      filterQuality: FilterQuality.high,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "${providerObj.productModelList[index].name}",
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      color: Colors.black,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "${providerObj.productModelList[index].price}",
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      color: Colors.black,
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Provider.of<ProductController>(context, listen: false)
                              .addFavourite(index: index);
                          // if (providerObj.productModelList[index].favourite) {
                          //   Provider.of<ProductController>(context,
                          //           listen: false)
                          //       .addWish(
                          //           obj: providerObj.productModelList[index]);
                          // } else {
                          Provider.of<ProductController>(context, listen: false)
                              .removeWish(obj: index);
                        },
                        child: Icon(
                          providerObj.productModelList[index].favourite
                              ? Icons.favorite_outlined
                              : Icons.favorite_border_rounded,
                          color: Colors.red,
                        ),
                      ),
                      const Spacer(),
                      GestureDetector(
                        onTap: () {
                          Provider.of<ProductController>(context, listen: false)
                              .incrementCount(index);
                        },
                        child: const CircleAvatar(
                          backgroundColor: Colors.blue,
                          radius: 20,
                          child: Icon(
                            Icons.add,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Text(
                          "${providerObj.productModelList[index].count}",
                          style: const TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 15,
                            color: Colors.black,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 0,
                      ),
                      GestureDetector(
                        onTap: () {
                          Provider.of<ProductController>(context, listen: false)
                              .decrementCount(index);
                        },
                        child: const CircleAvatar(
                          backgroundColor: Colors.blue,
                          radius: 20,
                          child: Icon(
                            Icons.remove,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            );
          }),
    );
  }
}
