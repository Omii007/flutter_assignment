import 'package:flutter/material.dart';
import 'package:product/controller/provider_controller.dart';
import 'package:provider/provider.dart';

import 'view/product_screen.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) {
        return ProductController();
      },
      child: const MaterialApp(
        home: ProductScreen(),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
