import 'package:flutter/material.dart';

class SharedData extends InheritedWidget {
  final int data;

  const SharedData({
    super.key,
    required this.data,
    required super.child,
  });

  static SharedData of(BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<SharedData>()!;
  }

  @override
  bool updateShouldNotify(SharedData oldWidget){
    return data != oldWidget.data;
  }
}
void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const SharedData(
      data: 7, 
      child: MaterialApp(
        home: MyApp(),
        debugShowCheckedModeBanner:false,
      ),
    );
  }
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State createState()=> _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {

    SharedData sharedDataObj = SharedData.of(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Inherited Widget State",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.blue,
      ),
      body: Column(
        children: [
          Text("${sharedDataObj.data}"),

          const SizedBox(
            height: 20,
          ),
          const AccessDataWidget(),
        ],
      ),
    );
  }
}

class AccessDataWidget extends StatelessWidget {
  const AccessDataWidget({super.key});

  @override
  Widget build(BuildContext context) {

    SharedData sharedData = SharedData.of(context);
    return Row(
      children: [
        Text("${sharedData.data}"),
        const SizedBox(
          height: 20,
          width: 50,
        ),

        const AccessDataChild(),
      ],
    );
  }
}

class AccessDataChild extends StatelessWidget {
  const AccessDataChild({super.key});

  @override
  Widget build(BuildContext context) {
    //SharedData sharedDataObj = context.dependOnInheritedWidgetOfExactType<SharedData>()!;
    SharedData sharedDataObj1 = SharedData.of(context);

    return Text("${sharedDataObj1.data}");
  }
}