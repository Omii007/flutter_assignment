import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Assignment(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class Assignment extends StatefulWidget {
  const Assignment({super.key});

  @override
  State createState(){
    print("In createState");
    return _AssignmentState();
  }
}

  class ClassModel {
    final String? question;
    final List<String?> options;
    final int? answer;
    const ClassModel({required this.question, required this.options, required this.answer});
  }

class _AssignmentState extends State {

  List questionList = [
    const ClassModel(
      question: "Which company developed flutter?",
      options: ["Google","Microsoft","Apple","Tesla"],
      answer: 0,
    ),
    const ClassModel(
      question: "Elon Musk is which company founder?",
      options: ["Google","Microsoft","Apple","Tesla"],
      answer: 3,
    ),
  ];
  int questionIndex = 0;
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    print("In initState");
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("In did change dependencies");
  }

  @override
  void didUpdateWidget(covariant StatefulWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    print("In did update widget");
  }

  @override
  void deactivate() {
    super.deactivate();
    print("In deactive");
  }

  @override
  void dispose() {
    super.dispose();
    print("In dispose");
  }

  // void getDone(){
    
  // }

  // bool isScreen = true;
  // Widget isdeactivate(){
  //   if(isScreen){
  //     return Scaffold(
  //       appBar: AppBar(
  //         title: const Text("Life Cycle",
  //           style: TextStyle(
  //             fontWeight: FontWeight.bold,
  //             fontSize:25,
  //             color: Colors.black,
  //           ),
  //         ),
  //         backgroundColor: Colors.pink,
  //       ),
  //       body: Column(
  //         children: [
  //           SizedBox(
  //             height: 100,
  //             width: 383,
  //             child: Text(questionList[questionIndex].question,
  //               style: const TextStyle(
  //                 fontWeight: FontWeight.bold,
  //                 fontSize: 20,
  //                 color: Colors.black,
  //               ),
  //             ),
  //           ),
  //         ],
  //       ),
  //     );
  //   }else{
  //     return Scaffold();
  //   }
  // }

  @override
  Widget build(BuildContext context ){
    print("In build");
    return Scaffold(
      appBar: AppBar(
        title: const Text("TextField Demo",
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.orange,
      ),
      body: Column(
        children:[
          const SizedBox(
            height: 30,
          ),
          TextField(
            controller: _controller,
            decoration: const InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20)),
              )
            ),
          ),
          ElevatedButton(
            onPressed: (){
              setState(() {
                print("In setState");
              });
            }, 
            child: const Text("Submit"),
          ),

            const Text("In flutter",
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
        ],
      ),
    );
  }
}
