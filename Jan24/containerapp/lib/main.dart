import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ContainerApp(),
      debugShowCheckedModeBanner:false,
    );
  }
}

class ContainerApp extends StatefulWidget {
  const ContainerApp({super.key});

  @override
  State createState()=> _ContainerAppState();
}

class _ContainerAppState extends State {

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(
        child: Container(
          height: 200,
          width: 200,
          decoration:  BoxDecoration(
            // borderRadius: BorderRadius.circular(10),
            color: Colors.pink,
            shape: BoxShape.circle,
            border: Border.all(width: 2,color: const Color.fromRGBO(0, 0, 0, 1),),
            boxShadow:  const[
              BoxShadow(
                color: Color.fromRGBO(229, 132, 204, 1),
                blurRadius: 20,
                spreadRadius: 10,
                offset: Offset(0, 5),
              )
            ],
          ),
          child: 
             Image.network(
              "https://bcciplayerimages.s3.ap-south-1.amazonaws.com/ipl/IPLHeadshot2023/2.png",
              // height: 10,
              // width: 10 ,
            ),
          ),
        ),
      
    );
  }
}
