import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ContainerApp(),
      debugShowCheckedModeBanner:false,
    );
  }
}

class ContainerApp extends StatelessWidget {
  const ContainerApp({super.key});

  @override
  Widget build(BuildContext context){

    return Scaffold(
      body: Center(
        child: Container(
            padding:const EdgeInsets.only(
              left: 10,right: 10,top: 10,bottom: 10,
            ),
            margin: const EdgeInsets.only(top:10,bottom:10,left:10,right:10),
            height: 200,
            width: 200, 
            decoration: BoxDecoration(
              borderRadius:const BorderRadius.all(Radius.circular(20)),
              border:Border.all(
                color: Colors.black,
                width: 5,
              ),
              gradient:const LinearGradient(
                stops:[0.0,1.0],
                colors:[
                  Colors.orange,
                  Colors.white,
                  Colors.green,
                ],
                // begin: Alignment.topCenter,
                // end: Alignment.bottomCenter,
              ),
              color: Colors.amber,
              boxShadow:const [
                BoxShadow(
                  color: Colors.purple,offset:Offset(30, 30),blurRadius: 8,
                ),
                BoxShadow(
                  color:Colors.red,offset: Offset(20, 20),blurRadius: 8,
                ),
                BoxShadow(
                  color: Colors.green,offset: Offset(10, 10),blurRadius: 8,
                ),
              ],
            ),
          ),
      ),
    );
  }
}
