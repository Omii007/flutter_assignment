
import 'package:flutter/material.dart';

class Bajaj extends StatefulWidget {
  const Bajaj({super.key});

  @override
  State<Bajaj> createState() => _BajajState();
}

class _BajajState extends State<Bajaj> {

 @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          const Padding(padding: EdgeInsets.all(10)),
          Image.asset("assets/images/Bajaj Logo.webp"),
          const Spacer(),

          IconButton(
            onPressed: (){}, 
            icon: const Icon(
            Icons.notifications,
            color: Colors.black,
            size: 40,
            ),
          ),
          
          const SizedBox(
            width: 15,
          ),

          IconButton(
            onPressed: (){}, 
            icon: const Icon(
            Icons.person,
            color: Colors.black,
            size: 40,
            ),
          ),
          
        ],
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  const Padding(padding: EdgeInsets.all(5)),
                  SizedBox(
                    child: Image.network(
                      "https://www.otocapital.in/_next/image?url=https%3A%2F%2Fassets.otocapital.in%2Fprod%2Fburnt-red-bajaj-pulsar-160-image.jpeg&w=1536&q=75",
                      height: 300,
                      width: 400,
                    ),
                  ),
                  const Padding(padding: EdgeInsets.all(5)),
                  SizedBox(
                    child: Image.network(
                      "https://img.indianautosblog.com/2020/10/19/bajaj-pulsar-ns200-new-colour-f7c2.jpg",
                      height: 300,
                      width: 400,
                    ),
                  ),
                  const Padding(padding: EdgeInsets.all(5)),
                  SizedBox(
                    child: Image.network(
                      "https://c4.wallpaperflare.com/wallpaper/780/625/239/motorcycles-other-wallpaper-preview.jpg",
                      height: 300,
                      width: 400,
                    ),
                  ),
                  const Padding(padding: EdgeInsets.all(5)),
                ],
              ),
            ),
            const Padding(padding: EdgeInsets.only(top: 10)),
            Row(
              children: [
                const Padding(padding: EdgeInsets.all(5)),
                Container(
                  height: 100,
                  width: 190,
                  decoration:const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Colors.deepPurple),
                  // color: Colors.deepPurple,
                  child: TextButton.icon(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.miscellaneous_services,
                      color: Colors.white,
                      size: 40,
                    ), 
                    label: const Text("Services",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      ),
                    ),
                  )
                ),
                const Padding(padding: EdgeInsets.all(5)),
                Container(
                  height: 100,
                  width: 190,
                  decoration:const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Colors.deepPurple),
                  //color: Colors.deepPurple,
                  child: TextButton.icon(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.help_sharp,
                      size: 40,
                      color: Colors.white,
                    ), 
                    label: const Text("Help",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      ),
                    ),
                  )
                ),
              ],
            ),
            const Padding(padding: EdgeInsets.only(top: 10)),
            Row(
              children: [
                const Padding(padding: EdgeInsets.all(5)),
                Container(
                  height: 100,
                  width: 190,
                  decoration:const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Colors.deepPurple),
                  //color: Colors.deepPurple,
                  child: TextButton.icon(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.directions_bike_rounded,
                      size: 40,
                      color: Colors.white,
                    ), 
                    label: const Text("Bike",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      ),
                    ),
                    
                  )
                ),
                const Padding(padding: EdgeInsets.all(5)),
                Container(
                  height: 100,
                  width: 190,
                  decoration:const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Colors.deepPurple),
                  //color: Colors.deepPurple,
                  child: TextButton.icon(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.location_on,
                      size: 40,
                      color: Colors.white,
                    ), 
                    label: const Text("Location",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      ),
                    ),
                  )
                ),
              ],
            ),
            const Padding(padding: EdgeInsets.only(top: 10)),
            Row(
              children: [
                const Padding(padding: EdgeInsets.all(5)),
                Container(
                  height: 100,
                  width: 190,
                  decoration:const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Colors.deepPurple),
                  //color: Colors.deepPurple,
                  child: TextButton.icon(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.groups,
                      size: 40,
                      color: Colors.white,
                    ), 
                    label: const Text("Community",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      ),
                    ),
                  )
                ),
                const Padding(padding: EdgeInsets.all(5)),
                Container(
                  height: 100,
                  width: 190,
                  decoration:const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Colors.deepPurple),
                  //color: Colors.deepPurple,
                  child: TextButton.icon(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.price_check,
                      size: 40,
                      color: Colors.white,
                    ), 
                    label: const Text("Reward",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
                ),
              ],
            ),
            const Padding(padding: EdgeInsets.only(top: 10)),
            Row(
              children: [
                const Padding(padding: EdgeInsets.all(5)),
                Container(
                  height: 100,
                  width: 190,
                  decoration:const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Colors.deepPurple),
                  //color: Colors.deepPurple,
                  child: TextButton.icon(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.travel_explore_rounded,
                      size: 40,
                      color: Colors.white,
                    ), 
                    label: const Text("Explore",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      ),
                    ),
                  )
                ),
                const Padding(padding: EdgeInsets.all(5)),
                Container(
                  height: 100,
                  width: 190,
                  decoration:const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Colors.deepPurple),
                  //color: Colors.deepPurple,
                  child: TextButton.icon(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.feedback_rounded,
                      size: 40,
                      color: Colors.white,
                    ), 
                    label: const Text("Feedback",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}