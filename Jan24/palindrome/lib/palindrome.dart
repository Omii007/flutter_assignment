
import 'package:flutter/material.dart';

class Palindrome extends StatefulWidget {

  const Palindrome({super.key});
  @override
  State<Palindrome> createState() => _PalindromeState();
}

class _PalindromeState extends State<Palindrome> {

  int? selectIndex = 0;
  int _count = 0;
  int _count1 = 0;
  int _count2 = 0;

  final List integerList = [100,121,25,33,66,23,153,2,145,1];

  void nextNumber(){
    _count =0;
    setState(() {
      if(selectIndex == integerList.length-1){
      selectIndex = 0;
    }else{
      selectIndex =selectIndex! + 1;
    }
    });
    for(int i= 0;i<integerList.length;i++){
      int num = integerList[i];
      int num1 = num;
      int mult = 0;
      while(num1 != 0){
        int rem = num1 % 10;
        mult = mult * 10 + rem;
        num1 = num1 ~/ 10;
      }
      if(mult == num){
        _count++;
      }
    }
  }

    void armstrongNumber(){
      _count1 = 0;
      setState(() {
        if(selectIndex == integerList.length-1){
          selectIndex = 0;
        }else{
          selectIndex =selectIndex! + 1;
        }
    });  
    for(int i=0;i<integerList.length;i++){
        int num = integerList[i];
        int num1 = num;
        int num2 = num;
        int cnt = 0;
        while(num1 != 0){
          cnt++;
          num1 = num1 ~/ 10;
        }
        int sum = 0;
        while(num2 != 0){
          int rem = num2 % 10;
          int mult = 1;
          for(int j=1;j<= cnt;j++){
            mult = mult * rem;
          }
          sum = sum + mult;
          num2 = num2 ~/ 10;
        }
        if(sum == num){
          _count1++;
        }
      }
  }

  void strongNumber(){
    _count2 = 0;

    setState(() {
      if(selectIndex == integerList.length-1){
        selectIndex = 0;
      }else{
        selectIndex = selectIndex! + 1;
      }
    });

    for(int i=0;i<integerList.length;i++){
      int num = integerList[i];
      int num1 = num;
      int sum = 0;
      while(num1 != 0){
        int rem = num1 % 10;
        int mult = 1;
        for(int j=1;j<=rem;j++){
          mult = mult * j;
        }
        sum = sum + mult;
        num1 = num1 ~/ 10;
      }
      if(sum == num){
        _count2++;
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Palindrome Number"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Count of Palindrome number = $_count",
              style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: nextNumber, 
              child: const Text("Palindrome Count"),
            ),
            const SizedBox(
              height: 20,
            ),

            Text("Count of Armstrong number = $_count1",
              style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: armstrongNumber, 
              child: const Text("Armstrong Count"),
            ),

            const SizedBox(
              height: 20,
            ),
            Text("Count of Strong number = $_count2",
              style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: strongNumber, 
              child: const Text("Strong Count"),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: (){
                setState(() {
                  _count = 0;
                  _count1 = 0;
                  _count2 = 0;
                });
              }, 
              child: const Text("Reset"),
            ),
          ],
        ),
      ),
      
    );
  }
}