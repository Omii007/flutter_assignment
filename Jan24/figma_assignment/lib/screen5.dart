
import 'dart:async';

import 'package:figma_assignment/screen4.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class PlantInfo extends StatefulWidget {
  const PlantInfo({super.key});

  @override
  State createState()=> _PlantInfoState();
}

class _PlantInfoState extends State<PlantInfo> {

  List imageList = [
    "assets/images/plant.png",
    "assets/images/plant.png",
    "assets/images/plant.png",
  ];

  @override
  void initState() {
    super.initState();
    autoScroll();
  }

  void autoScroll(){
    timer = Timer.periodic(const Duration(seconds: 3), (timer) {
      if(_pageController.page == imageList.length-1){
        _pageController.animateToPage(
          0, 
          duration: const Duration(milliseconds: 300), 
          curve: Curves.easeInOut,
        );
      }else{
        _pageController.nextPage(
          duration: const Duration(milliseconds: 300), 
          curve: Curves.easeInOut,
        );
      }
     });
  }

  final PageController _pageController = PageController(initialPage: 0);
  int currentImage = 0;
  Timer? timer;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 16.0,top: 50),
            child: SizedBox(
              height: 24,
              width: 24,
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(
                    context, 
                    MaterialPageRoute(
                      builder: (context)=> const HomePage(),
                    ),
                  );
                },
                child: Image.asset(
                  "assets/icons/left.png",
                  height: 24,
                  width: 24,
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 18,
          ),
          
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 244,
                width: 195,
                child: PageView.builder(
                  controller: _pageController,
                  itemCount: imageList.length,
                  onPageChanged: (value) {
                    setState(() {
                      currentImage = value;
                    });
                  },
                  itemBuilder: (context, index) {
                    return SizedBox(
                      height: 244,
                      width: 195,
                      child: Image.asset(
                        imageList[index],
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: 
              List<Widget>.generate(imageList.length, (index) => Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4.0,vertical: 8),
                child: InkWell(
                  onTap: () {
                    _pageController.animateToPage(
                      index, 
                      duration: const Duration(milliseconds: 300), 
                      curve: Curves.easeIn,
                    );
                  },
                  child: CircleAvatar(
                    radius: 4,
                    backgroundColor: currentImage == index?const Color.fromRGBO(62, 102, 24, 1): const Color.fromRGBO(197, 214, 181, 1),
                  ),
                ),
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 10.0,left: 31),
            child: SizedBox(
              height: 33,
              width: 150,
              child: Text("Snake Plants",
                style: GoogleFonts.poppins(
                  fontWeight:FontWeight.w600,
                  fontSize:22,
                  color:const Color.fromRGBO(48,48,48,1),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0,left: 31),
            child: SizedBox(
              height: 40,
              width: 298,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Plansts make your life with minimal and",
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w400,
                      fontSize:13,
                      color:const Color.fromRGBO(48,48,48,1),
                    ),
                  ),
                  Text("happy love the plants more and enjoy life.",
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w400,
                      fontSize:13,
                      color:const Color.fromRGBO(48,48,48,1),
                    ),
                  ),
                ],
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 37.0,left: 20,right: 20),
            child: Container(
              height: 200,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: const Color.fromRGBO(118, 152, 75, 1),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0,top: 30,right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        // 1
                        Padding(
                          padding: const EdgeInsets.only(left: 0.0,top: 0),
                          child: SizedBox(
                            height: 74,
                            width: 64,
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 20,
                                  width: 24,
                                  child: Image.asset(
                                    "assets/icons/height.png",
                                    height: 20,
                                    width: 24,
                                    color: const Color.fromRGBO(255,255,255,1),
                                    filterQuality: FilterQuality.high,
                                    fit: BoxFit.fitHeight,
                                  ),
                                ),
                                const SizedBox(
                                  height: 16,
                                ),
                                SizedBox(
                                  height: 18,
                                  width: 40,
                                  child: Text("Height",
                                    style: GoogleFonts.poppins(
                                      fontWeight:FontWeight.w500,
                                      fontSize:12,
                                      color:const Color.fromRGBO(255,255,255,1),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                SizedBox(
                                  height: 15,
                                  width: 64,
                                  child: Text("30cm-40cm",
                                    style: GoogleFonts.poppins(
                                      fontWeight:FontWeight.w500,
                                      fontSize:10,
                                      color:const Color.fromRGBO(255,255,255,0.6),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        // 2
                        Padding(
                          padding: const EdgeInsets.only(left: 0.0,top: 0),
                          child: SizedBox(
                            height: 74,
                            width: 64,
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 20,
                                  width: 24,
                                  child: Image.asset(
                                    "assets/icons/thermometer.png",
                                    height: 20,
                                    width: 24,
                                    color: const Color.fromRGBO(255,255,255,1),
                                    filterQuality: FilterQuality.high,
                                    fit: BoxFit.fitHeight,
                                  ),
                                ),
                                const SizedBox(
                                  height: 16,
                                ),
                                SizedBox(
                                  height: 18,
                                  width: 100,
                                  child: Text("Temperature",
                                    style: GoogleFonts.poppins(
                                      fontWeight:FontWeight.w500,
                                      fontSize:12,
                                      color:const Color.fromRGBO(255,255,255,1),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                SizedBox(
                                  height: 15,
                                  width: 64,
                                  child: Text("20’C-25’C",
                                    style: GoogleFonts.poppins(
                                      fontWeight:FontWeight.w500,
                                      fontSize:10,
                                      color:const Color.fromRGBO(255,255,255,0.6),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        // 3
                        Padding(
                          padding: const EdgeInsets.only(left: 0.0,top: 0),
                          child: SizedBox(
                            height: 74,
                            width: 64,
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 20,
                                  width: 24,
                                  child: Image.asset(
                                    "assets/icons/pot.png",
                                    height: 20,
                                    width: 24,
                                    color: const Color.fromRGBO(255,255,255,1),
                                    filterQuality: FilterQuality.high,
                                    fit: BoxFit.fitHeight,
                                  ),
                                ),
                                const SizedBox(
                                  height: 16,
                                ),
                                SizedBox(
                                  height: 18,
                                  width: 40,
                                  child: Text("Pot",
                                    style: GoogleFonts.poppins(
                                      fontWeight:FontWeight.w500,
                                      fontSize:12,
                                      color:const Color.fromRGBO(255,255,255,1),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                SizedBox(
                                  height: 15,
                                  width: 64,
                                  child: Text("Ciramic Pot",
                                    style: GoogleFonts.poppins(
                                      fontWeight:FontWeight.w500,
                                      fontSize:10,
                                      color:const Color.fromRGBO(255,255,255,0.6),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 30.0,left: 30),
                    child: SizedBox(
                      height: 21,
                      width: 77,
                      child: Text("Total Price",
                        style: GoogleFonts.poppins(
                          fontWeight:FontWeight.w500,
                          fontSize:14,
                          color:const Color.fromRGBO(255,255,255,0.8),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 0.0,left: 30),
                    child: SizedBox(
                      height: 21,
                      width: 77,
                      child: Text("₹ 350",
                        style: GoogleFonts.poppins(
                          fontWeight:FontWeight.w600,
                          fontSize:18,
                          color:const Color.fromRGBO(255,255,255,1),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}