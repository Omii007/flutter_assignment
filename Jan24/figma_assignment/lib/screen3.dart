

import 'package:figma_assignment/screen2.dart';
import 'package:figma_assignment/screen4.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Verification extends StatefulWidget {
  const Verification({super.key});

  @override
  State createState()=> _VerificationState();
}

class _VerificationState extends State<Verification> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // const SizedBox(
          //   height: 20,
          // ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0,top: 15),
            child: Row(  
              mainAxisAlignment: MainAxisAlignment.start,         
              children: [             
                IconButton(
                    onPressed: (){
                      Navigator.pop(
                        context, 
                        MaterialPageRoute(
                          builder: (context)=> const LoginPage(),
                        ),
                      );
                    }, 
                    icon: const ImageIcon(
                      AssetImage(
                        "assets/icons/backicon.png",
                      ),
                      size: 16,
                      color: Color.fromRGBO(0,0,0,1),
                    ),
                  ),
                
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.only(top: 0,left: 0),
                  child: SizedBox(      
                    child: Image.asset(
                      "assets/images/screen3.png",
                      height: 128.2,
                      width: 210,
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                ),
              ],
            ),
          ),
          
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text("Verification",
              style: GoogleFonts.poppins(
                fontWeight:FontWeight.w700,
                fontSize:20,
                color:const Color.fromRGBO(0,0,0,1),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: SizedBox(
              height: 46,
              width: 300,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text("Enter the OTP code from the phone we",
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w400,
                      fontSize:14,
                      color:const Color.fromRGBO(0,0,0,0.6),
                    ),
                  ),
                  Text("just sent you.",
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w400,
                      fontSize:14,
                      color:const Color.fromRGBO(0,0,0,0.6),
                    ),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.only(left:20.0,right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                // 1
                Container(
                  height: 56,
                  width: 56,
                  decoration: BoxDecoration(
                    color: const Color.fromRGBO(255,255,255,1),
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(
                      color: const Color.fromRGBO(204, 211, 196, 1),
                      width: 1,
                    ),
                    boxShadow: const[
                      BoxShadow(
                        offset: Offset(0, 8),
                        blurRadius: 20,
                        color: Color.fromRGBO(0,0,0,0.06),
                      ),
                    ],
                  ),
                ),
                // 2
                Container(
                  height: 56,
                  width: 56,
                  decoration: BoxDecoration(
                    color: const Color.fromRGBO(255,255,255,1),
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(
                      color: const Color.fromRGBO(204, 211, 196, 1),
                      width: 1,
                    ),
                    boxShadow: const[
                      BoxShadow(
                        offset: Offset(0, 8),
                        blurRadius: 20,
                        color: Color.fromRGBO(0,0,0,0.06),
                      ),
                    ],
                  ),
                ),
                // 3
                Container(
                  height: 56,
                  width: 56,
                  decoration: BoxDecoration(
                    color: const Color.fromRGBO(255,255,255,1),
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(
                      color: const Color.fromRGBO(204, 211, 196, 1),
                      width: 1,
                    ),
                    boxShadow: const[
                      BoxShadow(
                        offset: Offset(0, 8),
                        blurRadius: 20,
                        color: Color.fromRGBO(0,0,0,0.06),
                      ),
                    ],
                  ),
                ),
                // 4
                Container(
                  height: 56,
                  width: 56,
                  decoration: BoxDecoration(
                    color: const Color.fromRGBO(255,255,255,1),
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(
                      color: const Color.fromRGBO(204, 211, 196, 1),
                      width: 1,
                    ),
                    boxShadow: const[
                      BoxShadow(
                        offset: Offset(0, 8),
                        blurRadius: 20,
                        color: Color.fromRGBO(0,0,0,0.06),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

          const SizedBox(
            height: 30,
          ),

          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Row(
              children: [
                Text("Don’t receive OTP code!",
                  style: GoogleFonts.poppins(
                    fontWeight:FontWeight.w400,
                    fontSize:14,
                    color:const Color.fromRGBO(0,0,0,0.5),
                  ),
                ),
                TextButton(
                  onPressed: (){}, 
                  child: Text("Resend",
                    style: GoogleFonts.poppins(
                      fontWeight:FontWeight.w500,
                      fontSize:14,
                      color:const Color.fromRGBO(0,0,0,1),
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0,right: 20),
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                  context, 
                  MaterialPageRoute(
                    builder: (context)=> const HomePage(),
                  ),
                );
              },
              child: Container(
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(62, 102, 24, 1),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: const[
                    BoxShadow(
                      offset: Offset(0, 20),
                      blurRadius: 40,
                      color: Color.fromRGBO(0,0,0,0.15),
                    ),
                  ],
                ),
                child: SizedBox(
                  height: 32,
                  width: 135,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Submit ",
                      textAlign: TextAlign.center,
                        style: GoogleFonts.rubik(
                          fontWeight: FontWeight.w500,
                          fontSize:18,
                          color:const Color.fromRGBO(255,255,255,1),
                        ),
                      ),
                      
                    ],
                  ),
                ),
              ),
            ),
          ),

          
        ],
      ),
    );
  }
}