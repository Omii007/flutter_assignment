
import 'package:figma_assignment/screen2.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LandingPage extends StatefulWidget {
  const LandingPage({super.key});

  @override
  State createState()=> _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(255,255,255,1),
      body: Column(
        children: [
          const SizedBox(
            height: 44,
          ),
          Image.asset(
            "assets/images/image 2.png",
            fit: BoxFit.cover,
            height: 400,
            width: double.infinity,
          ),
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 56.66,right: 56.66),
            child: SizedBox(
              height: 100,
              width: 270,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text("Enjoy your",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400,
                          fontSize:25,
                          color:const Color.fromRGBO(47,47,47,1)
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text("life with ",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400,
                          fontSize:25,
                          color:const Color.fromRGBO(47,47,47,1)
                        ),
                      ),
                      Text("Plants ",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500,
                          fontSize:25,
                          color:const Color.fromRGBO(0,0,0,1)
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 40,
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                  context, 
                  MaterialPageRoute(
                    builder: (context)=> const LoginPage(),
                  ),
                );
              },
              child: Container(
                height: 50,
                width: 320,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(62, 102, 24, 1),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: const[
                    BoxShadow(
                      offset: Offset(0, 20),
                      blurRadius: 40,
                      color: Color.fromRGBO(0,0,0,0.15),
                    ),
                  ],
                ),
                child: SizedBox(
                  height: 32,
                  width: 135,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Get Started ",
                      textAlign: TextAlign.center,
                        style: GoogleFonts.rubik(
                          fontWeight: FontWeight.w500,
                          fontSize:18,
                          color:const Color.fromRGBO(255,255,255,1),
                        ),
                      ),
                      Image.asset(
                        "assets/icons/forward.png",
                        height: 24,
                        width: 24,
                        color: const Color.fromRGBO(255,255,255,1),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}