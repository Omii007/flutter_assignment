import 'package:figma_assignment/screen3.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State createState()=> _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView( 
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 0,left: 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(      
                  child: Image.asset(
                    "assets/images/screen2.png",
                    height: 128.2,
                    width: 210,
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Log in",
                style: GoogleFonts.poppins(
                  fontWeight:FontWeight.w500,
                  fontSize:30,
                  color:const Color.fromRGBO(0,0,0,1),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: TextFormField(
              decoration: InputDecoration(
                prefixIcon: const ImageIcon(
                  AssetImage(
                    "assets/icons/call.png",
                  ),
                  size: 17,
                  color:  Color.fromRGBO(164, 164, 164, 1), 
                ),
                hintText: "Mobile Number",
                hintStyle: GoogleFonts.inter(
                  fontWeight:FontWeight.w400,
                  color:  const Color.fromRGBO(164, 164, 164, 1),
                  fontSize:13 
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: const BorderSide(
                    color: Color.fromRGBO(204, 211, 196, 1),
                    width: 1,
                ),
              ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: const BorderSide(
                    color: Color.fromRGBO(204, 211, 196, 1),
                    width: 1,
                ),
              ),
              
            ),
          ),
          ),
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                  context, 
                  MaterialPageRoute(
                    builder: (context)=> const Verification(),
                  ),
                );
              },
              child: Container(
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(62, 102, 24, 1),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: const[
                    BoxShadow(
                      offset: Offset(0, 20),
                      blurRadius: 40,
                      color: Color.fromRGBO(0,0,0,0.15),
                    ),
                  ],
                ),
                child: SizedBox(
                  height: 32,
                  width: 135,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Log in ",
                      textAlign: TextAlign.center,
                        style: GoogleFonts.rubik(
                          fontWeight: FontWeight.w500,
                          fontSize:18,
                          color:const Color.fromRGBO(255,255,255,1),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 53,
          ),
          SizedBox(
            height: 280,
            child: Image.asset(
              "assets/images/plant2.png",
              width: double.infinity,
              filterQuality: FilterQuality.high,
              fit: BoxFit.fitHeight,
            ),
          ),
        ],
      ),
    );
  }
}
