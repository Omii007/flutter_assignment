
import 'dart:async';

import 'package:figma_assignment/screen5.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State createState()=> _HomePageState();
}
class ListModelClass{
  String title;
  String date;
  String image;

  ListModelClass({
    required this.title,
    required this.date,
    required this.image,
  });
}

class _HomePageState extends State<HomePage> {

  List cardList = [
    ListModelClass(
      title: "30% OFF", 
      date: "06-24 April", 
      image: "assets/images/image.png",
    ),
    ListModelClass(
      title: "30% OFF", 
      date: "06-24 April", 
      image: "assets/images/image.png",
    ),
    ListModelClass(
      title: "30% OFF", 
      date: "06-24 April", 
      image: "assets/images/image.png",
    ),
  ];

  @override
  void initState() {
    super.initState();
    startScroll();
  }

  void startScroll(){
    timer = Timer.periodic(const Duration(seconds: 3), (timer) {
      if(_pageController.page == cardList.length-1){
        _pageController.animateToPage(
          0, 
          duration: const Duration(milliseconds: 300), 
          curve: Curves.easeInOut,
        );
      }else{
        _pageController.nextPage(
          duration: const Duration(milliseconds: 300), 
          curve: Curves.easeInOut,
        );
      }
     });
  }

  final PageController _pageController = PageController(initialPage: 0);
  int activeImage = 0;
  Timer? timer;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(left: 20.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  const Spacer(),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: SizedBox(
                      height: 81,
                      width: 254,
                      child: Image.asset(
                        "assets/images/screen4.png",
                        filterQuality: FilterQuality.high,
                        fit:BoxFit.cover,
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Row(             
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 5.0),
                    child: SizedBox(
                      height: 68,
                      width: 180,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Find your",
                            style: GoogleFonts.poppins(
                              fontWeight:FontWeight.w500,
                              fontSize:24,
                              color:const Color.fromRGBO(0,0,0,1),
                            ),
                          ),
                          Text("favorite plants",
                            style: GoogleFonts.poppins(
                              fontWeight:FontWeight.w500,
                              fontSize:24,
                              color:const Color.fromRGBO(0,0,0,1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const Spacer(),
                  Container(
                    // height: 40,
                    // width: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: const Color.fromRGBO(255,255,255,1),
                    ),
                    child: Image.asset(
                      "assets/icons/bag.png",
                      height: 20,
                      width: 20,
                      fit: BoxFit.cover,
                      filterQuality: FilterQuality.high,
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              SizedBox(
                height: 108,
                width: 370,
                child: PageView.builder(
                  controller: _pageController,
                  itemCount: cardList.length,
                  onPageChanged: (value) {
                    setState(() {
                      activeImage = value;
                    });
                  },
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: Container(
                        height: 108,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: const Color.fromRGBO(204, 231, 185, 1),
                        ),
                        child: Row(
                          children: [
                            Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 25.0,top: 20),
                                  child: SizedBox(
                                    height: 32,
                                    width: 102,
                                    child: Text(cardList[index].title,
                                      style: GoogleFonts.poppins(
                                        fontWeight:FontWeight.w600,
                                        fontSize:24,
                                        color:const Color.fromRGBO(0,0,0,1),
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 25.0),
                                  child: SizedBox(
                                    height: 15,
                                    width: 100,
                                    child: Text(cardList[index].date,
                                      style: GoogleFonts.poppins(
                                        fontWeight:FontWeight.w400,
                                        fontSize:14,
                                        color:const Color.fromRGBO(0,0,0,0.6),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              width: 70,
                            ),
                            Image.asset(
                              cardList[index].image,
                              cacheHeight: 108,
                              cacheWidth: 110,
                              filterQuality: FilterQuality.high,
                              fit: BoxFit.contain,
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: 
                  List<Widget>.generate(cardList.length, (index) =>
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 4.0,vertical: 8),
                      child: InkWell(
                        onTap: () {
                          _pageController.animateToPage(
                            index, duration: const Duration(milliseconds: 300), 
                            curve: Curves.easeIn,
                          );
                        },
                        child: CircleAvatar(
                          radius: 4,
                          backgroundColor: activeImage == index? const Color.fromRGBO(62, 102, 24, 1): const Color.fromRGBO(197, 214, 181, 1),
                        ),
                      ),
                    ), 
                  ),
              ),
          
              Padding(
                padding: const EdgeInsets.only(left: 25.0,top: 10),
                child: Text("Indoor",
                  style: GoogleFonts.poppins(
                    fontWeight:FontWeight.w500,
                    fontSize:20,
                    color: const Color.fromRGBO(0,0,0,1),
                  ),
                ),
              ),
          
              Padding(
                padding: const EdgeInsets.only(left: 5.0,top: 10),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      // 1
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context, 
                            MaterialPageRoute(
                              builder: (context)=> const PlantInfo(),
                            ),
                          );
                        },
                        child: Container(
                          height: 188,
                          width: 141,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(9.4),
                            color: const Color.fromRGBO(255,255,255,1),
                            boxShadow: const [
                              BoxShadow(
                                offset: Offset(0, 7.52),
                                blurRadius: 18.8,
                                color:Color.fromRGBO(0,0,0,0.06),
                              ),
                            ],
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 26.0),
                                child: Image.asset(
                                  "assets/images/plant.png",
                                  height: 112.8,
                                  width: 90.24,
                                  filterQuality: FilterQuality.high,
                                  fit: BoxFit.fitHeight,
                                  alignment: Alignment.topCenter,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0,left: 15),
                                child: SizedBox(
                                  height: 17,
                                  width: 80,
                                  child: Text("Snake Plants",
                                    style: GoogleFonts.dmSans(
                                      fontWeight:FontWeight.w400,
                                      fontSize:13.16,
                                      color:const Color.fromRGBO(48,48,48,1),
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 7,
                              ),
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 15.0),
                                    child: Text("₹350",
                                      style: GoogleFonts.poppins(
                                        fontWeight:FontWeight.w600,
                                        fontSize:16.92,
                                        color:const Color.fromRGBO(62,102,24,1),
                                      ),
                                    ),
                                  ),
                                  const Spacer(),
                                  Container(
                                    // height: 26,
                                    // width: 26,
                                    decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.grey,
                                    ),
                                    child: Container(
                                      height: 14,
                                      width: 14,
                                      color: const Color.fromRGBO(255,255,255,1),
                                      child: Image.asset(
                                        "assets/icons/bag.png",
                                        height: 14,
                                        width: 14,
                                        alignment: Alignment.center,
                                        color: const Color.fromRGBO(0,0,0,1),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 15,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      // 2
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context, 
                            MaterialPageRoute(
                              builder: (context)=> const PlantInfo(),
                            ),
                          );
                        },
                        child: Container(
                          height: 188,
                          width: 141,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(9.4),
                            color: const Color.fromRGBO(255,255,255,1),
                            boxShadow: const [
                              BoxShadow(
                                offset: Offset(0, 7.52),
                                blurRadius: 18.8,
                                color:Color.fromRGBO(0,0,0,0.06),
                              ),
                            ],
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 26.0),
                                child: Image.asset(
                                  "assets/images/plant.png",
                                  height: 112.8,
                                  width: 90.24,
                                  filterQuality: FilterQuality.high,
                                  fit: BoxFit.fitHeight,
                                  alignment: Alignment.topCenter,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0,left: 15),
                                child: SizedBox(
                                  height: 17,
                                  width: 80,
                                  child: Text("Snake Plants",
                                    style: GoogleFonts.dmSans(
                                      fontWeight:FontWeight.w400,
                                      fontSize:13.16,
                                      color:const Color.fromRGBO(48,48,48,1),
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 7,
                              ),
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 15.0),
                                    child: Text("₹350",
                                      style: GoogleFonts.poppins(
                                        fontWeight:FontWeight.w600,
                                        fontSize:16.92,
                                        color:const Color.fromRGBO(62,102,24,1),
                                      ),
                                    ),
                                  ),
                                  const Spacer(),
                                  Container(
                                    // height: 26,
                                    // width: 26,
                                    decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.grey,
                                    ),
                                    child: Container(
                                      height: 14,
                                      width: 14,
                                      color: const Color.fromRGBO(255,255,255,1),
                                      child: Image.asset(
                                        "assets/icons/bag.png",
                                        height: 14,
                                        width: 14,
                                        alignment: Alignment.center,
                                        color: const Color.fromRGBO(0,0,0,1),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 15,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      // 3
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context, 
                            MaterialPageRoute(
                              builder: (context)=> const PlantInfo(),
                            ),
                          );
                        },
                        child: Container(
                          height: 188,
                          width: 141,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(9.4),
                            color: const Color.fromRGBO(255,255,255,1),
                            boxShadow: const [
                              BoxShadow(
                                offset: Offset(0, 7.52),
                                blurRadius: 18.8,
                                color:Color.fromRGBO(0,0,0,0.06),
                              ),
                            ],
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 26.0),
                                child: Image.asset(
                                  "assets/images/plant.png",
                                  height: 112.8,
                                  width: 90.24,
                                  filterQuality: FilterQuality.high,
                                  fit: BoxFit.fitHeight,
                                  alignment: Alignment.topCenter,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0,left: 15),
                                child: SizedBox(
                                  height: 17,
                                  width: 80,
                                  child: Text("Snake Plants",
                                    style: GoogleFonts.dmSans(
                                      fontWeight:FontWeight.w400,
                                      fontSize:13.16,
                                      color:const Color.fromRGBO(48,48,48,1),
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 7,
                              ),
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 15.0),
                                    child: Text("₹350",
                                      style: GoogleFonts.poppins(
                                        fontWeight:FontWeight.w600,
                                        fontSize:16.92,
                                        color:const Color.fromRGBO(62,102,24,1),
                                      ),
                                    ),
                                  ),
                                  const Spacer(),
                                  Container(
                                    // height: 26,
                                    // width: 26,
                                    decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.grey,
                                    ),
                                    child: Container(
                                      height: 14,
                                      width: 14,
                                      color: const Color.fromRGBO(255,255,255,1),
                                      child: Image.asset(
                                        "assets/icons/bag.png",
                                        height: 14,
                                        width: 14,
                                        alignment: Alignment.center,
                                        color: const Color.fromRGBO(0,0,0,1),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 15,
                                  ),
                                ],
                              ),
                              
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  width: 370,
                  decoration: const BoxDecoration(
                    border: Border.symmetric(
                      horizontal: BorderSide(
                        color: Color.fromRGBO(204, 211, 196, 1),
                      ),
                    ),
                  ),
                ),
              ),
              // 2nd row
          
              Padding(
                padding: const EdgeInsets.only(left: 25.0,top: 10),
                child: Text("Outdoor",
                  style: GoogleFonts.poppins(
                    fontWeight:FontWeight.w500,
                    fontSize:20,
                    color: const Color.fromRGBO(0,0,0,1),
                  ),
                ),
              ),
          
              Padding(
                padding: const EdgeInsets.only(left: 5.0,top: 10),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      // 1
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context, 
                            MaterialPageRoute(
                              builder: (context)=> const PlantInfo(),
                            ),
                          );
                        },
                        child: Container(
                          height: 188,
                          width: 141,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(9.4),
                            color: const Color.fromRGBO(255,255,255,1),
                            boxShadow: const [
                              BoxShadow(
                                offset: Offset(0, 7.52),
                                blurRadius: 18.8,
                                color:Color.fromRGBO(0,0,0,0.06),
                              ),
                            ],
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 26.0),
                                child: Image.asset(
                                  "assets/images/plant.png",
                                  height: 112.8,
                                  width: 90.24,
                                  filterQuality: FilterQuality.high,
                                  fit: BoxFit.fitHeight,
                                  alignment: Alignment.topCenter,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0,left: 15),
                                child: SizedBox(
                                  height: 17,
                                  width: 80,
                                  child: Text("Snake Plants",
                                    style: GoogleFonts.dmSans(
                                      fontWeight:FontWeight.w400,
                                      fontSize:13.16,
                                      color:const Color.fromRGBO(48,48,48,1),
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 7,
                              ),
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 15.0),
                                    child: Text("₹350",
                                      style: GoogleFonts.poppins(
                                        fontWeight:FontWeight.w600,
                                        fontSize:16.92,
                                        color:const Color.fromRGBO(62,102,24,1),
                                      ),
                                    ),
                                  ),
                                  const Spacer(),
                                  Container(
                                    // height: 26,
                                    // width: 26,
                                    decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.grey,
                                    ),
                                    child: Container(
                                      height: 14,
                                      width: 14,
                                      color: const Color.fromRGBO(255,255,255,1),
                                      child: Image.asset(
                                        "assets/icons/bag.png",
                                        height: 14,
                                        width: 14,
                                        alignment: Alignment.center,
                                        color: const Color.fromRGBO(0,0,0,1),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 15,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      // 2
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context, 
                            MaterialPageRoute(
                              builder: (context)=> const PlantInfo(),
                            ),
                          );
                        },
                        child: Container(
                          height: 188,
                          width: 141,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(9.4),
                            color: const Color.fromRGBO(255,255,255,1),
                            boxShadow: const [
                              BoxShadow(
                                offset: Offset(0, 7.52),
                                blurRadius: 18.8,
                                color:Color.fromRGBO(0,0,0,0.06),
                              ),
                            ],
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 26.0),
                                child: Image.asset(
                                  "assets/images/plant.png",
                                  height: 112.8,
                                  width: 90.24,
                                  filterQuality: FilterQuality.high,
                                  fit: BoxFit.fitHeight,
                                  alignment: Alignment.topCenter,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0,left: 15),
                                child: SizedBox(
                                  height: 17,
                                  width: 80,
                                  child: Text("Snake Plants",
                                    style: GoogleFonts.dmSans(
                                      fontWeight:FontWeight.w400,
                                      fontSize:13.16,
                                      color:const Color.fromRGBO(48,48,48,1),
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 7,
                              ),
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 15.0),
                                    child: Text("₹350",
                                      style: GoogleFonts.poppins(
                                        fontWeight:FontWeight.w600,
                                        fontSize:16.92,
                                        color:const Color.fromRGBO(62,102,24,1),
                                      ),
                                    ),
                                  ),
                                  const Spacer(),
                                  Container(
                                    // height: 26,
                                    // width: 26,
                                    decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.grey,
                                    ),
                                    child: Container(
                                      height: 14,
                                      width: 14,
                                      color: const Color.fromRGBO(255,255,255,1),
                                      child: Image.asset(
                                        "assets/icons/bag.png",
                                        height: 14,
                                        width: 14,
                                        alignment: Alignment.center,
                                        color: const Color.fromRGBO(0,0,0,1),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 15,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      // 3
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context, 
                            MaterialPageRoute(
                              builder: (context)=> const PlantInfo(),
                            ),
                          );
                        },
                        child: Container(
                          height: 188,
                          width: 141,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(9.4),
                            color: const Color.fromRGBO(255,255,255,1),
                            boxShadow: const [
                              BoxShadow(
                                offset: Offset(0, 7.52),
                                blurRadius: 18.8,
                                color:Color.fromRGBO(0,0,0,0.06),
                              ),
                            ],
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 26.0),
                                child: Image.asset(
                                  "assets/images/plant.png",
                                  height: 112.8,
                                  width: 90.24,
                                  filterQuality: FilterQuality.high,
                                  fit: BoxFit.fitHeight,
                                  alignment: Alignment.topCenter,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0,left: 15),
                                child: SizedBox(
                                  height: 17,
                                  width: 80,
                                  child: Text("Snake Plants",
                                    style: GoogleFonts.dmSans(
                                      fontWeight:FontWeight.w400,
                                      fontSize:13.16,
                                      color:const Color.fromRGBO(48,48,48,1),
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 7,
                              ),
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 15.0),
                                    child: Text("₹350",
                                      style: GoogleFonts.poppins(
                                        fontWeight:FontWeight.w600,
                                        fontSize:16.92,
                                        color:const Color.fromRGBO(62,102,24,1),
                                      ),
                                    ),
                                  ),
                                  const Spacer(),
                                  Container(
                                    // height: 26,
                                    // width: 26,
                                    decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.grey,
                                    ),
                                    child: Container(
                                      height: 14,
                                      width: 14,
                                      color: const Color.fromRGBO(255,255,255,1),
                                      child: Image.asset(
                                        "assets/icons/bag.png",
                                        height: 14,
                                        width: 14,
                                        alignment: Alignment.center,
                                        color: const Color.fromRGBO(0,0,0,1),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 15,
                                  ),
                                ],
                              ),
                              
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}