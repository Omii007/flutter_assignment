
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

dynamic database;

Future insertPlayerData(PlayerInfo obj) async{
  final localDB = await database;

  await localDB.insert(
    "PlayerInfo",
    obj.mapPlayerInfo(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

Future <List<PlayerInfo>> getPlayerData()async {
  final localDb = await database;

  List<Map<String,dynamic>> listPlayer = await localDb.query("PlayerInfo");

  return List.generate(listPlayer.length,(index){
    return PlayerInfo(
      name: listPlayer[index]['name'], 
      age: listPlayer[index]['age'], 
      avg: listPlayer[index]['avg'],
      jerNo: listPlayer[index]['jerNo'], 
      runs: listPlayer[index]['runs'],
    );
  });
}

class PlayerInfo{
  final String name;
  final int jerNo;
  final int age;
  final int runs;
  final double avg;

  PlayerInfo({
    required this.name,
    required this.age,
    required this.avg,
    required this.jerNo,
    required this.runs,
  });

  Map<String,dynamic> mapPlayerInfo(){
    return {
      'name': name,
      'age': age,
      'avg': avg,
      'jerNo': jerNo,
      'runs': runs,
    };
  }

  @override
  String toString(){
    return '{name: $name, jerNo: $jerNo, age: $age, runs: $runs, avg: $avg}';
  }
}
void main()async{
  runApp(const MainApp());

  database = openDatabase(
    join(await getDatabasesPath(),"CrcketerInfo.db"),
    version: 1,
    onCreate: (db, version) async{
      await db.execute(
        '''
          CREATE TABLE PlayerInfo(
            name TEXT,
            jerNo INT PRIMARY KEY,
            age INT,
            runs INT,
            avg REAL
          )
        '''
      );
    },
  );

  PlayerInfo obj = PlayerInfo(
    name: "Virat", 
    age: 35, 
    avg: 51.67, 
    jerNo: 18, 
    runs: 13000,
  );

  await insertPlayerData(obj);
  print(await getPlayerData());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Container(
            height: 10,
            width: 20,
            color: Colors.red,
          ),
        ),
      ),
    );
  }
}