import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: "Flutter Assignment",
      home: MyHomePage(
        title: "Add Names",
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final TextEditingController _namesTextEditingController = TextEditingController();
  final FocusNode _nameFocusNode = FocusNode();

  List  addList = [];
  // String _counter = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        foregroundColor: Colors.white,
        title: Text(widget.title),
      ),
      body: Column(
            children: [
              
              const SizedBox(
                height: 30,
              ),
              TextField(
                controller: _namesTextEditingController,
                focusNode: _nameFocusNode,
                decoration: InputDecoration(
                  hintText: "Enter name",
                  border: InputBorder.none,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30),
                    borderSide: const BorderSide(
                      color: Colors.black,width: 2,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30),
                    borderSide: const BorderSide(
                      color: Colors.pink,
                    ),
                  ),
                ),
                cursorColor: Colors.orange,
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.phone,
                onChanged: (value) {
                  print("Value = $value");
                },
                onSubmitted: (value){
                  setState(() {
                    addList.add(value);
                  });
                },
              ),
              const SizedBox(
                height: 30,
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: addList.length,
                  itemBuilder: (context,index){
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: 50,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: const Color.fromARGB(255, 118, 178, 228),
                          boxShadow: const[
                            BoxShadow(
                              color: Colors.red,offset: Offset(10, 10),blurRadius: 10,
                            )
                          ],
                        ),
                        
                        child: Center(
                          child: Text(addList[index],
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 30,
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
             
          ],  
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: ( () {
            setState(() {
              addList.add(_namesTextEditingController.text);
            });
          }),
          child: const Text("ADD"),
        ),  
    );
  }
}
