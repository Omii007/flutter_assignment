import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Flutter Assignment",
      home: MyHomePage(
        title: "Add Names",
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final TextEditingController _namesTextEditingController = TextEditingController();
  final FocusNode _nameFocusNode = FocusNode();

  List  addList = [];
  // String _counter = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        foregroundColor: Colors.white,
        title: Text(widget.title),
      ),
      body: ListView.builder(
        itemCount: addList.length,
        itemBuilder: (context,index){
          return Column(
            children: [
              const SizedBox(
                height: 30,
              ),
              TextField(
                controller: _namesTextEditingController,
                focusNode: _nameFocusNode,
                decoration: InputDecoration(
                  hintText: "Enter name",
                  border: InputBorder.none,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30),
                    borderSide: const BorderSide(
                      color: Colors.blue,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30),
                    borderSide: const BorderSide(
                      color: Colors.pink,
                    ),
                  ),
                ),
                cursorColor: Colors.orange,
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.phone,
                onChanged: (value) {
                  print("Value = $value");
                },
                onSubmitted: (value){
                  setState(() {
                    addList.add(value);
                  });
                },
              ),
            ],
          );
        },
        
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          // SizedBox(
          //   height: 50,
          //   width: 500,
          //   child: Text("$_counter"),
          // );
          setState(() {

            addList.add(_namesTextEditingController);
          });
        }),
    );
  }
}
