
import 'package:flutter/material.dart';

class Assignment4 extends StatefulWidget {
  const Assignment4({super.key});

  @override
  State<Assignment4> createState() => _Assignment4State();
}

class _Assignment4State extends State<Assignment4>{

  bool c1 = false;
  bool liked1 = false;
  bool liked2 = false;
  bool liked3 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Instagram",
          style: TextStyle(
            fontStyle: FontStyle.italic,
            color: Colors.black,
            fontSize: 30,
          ),
        ),
        actions: const [
          Icon(
            Icons.favorite_border_rounded,
            color: Colors.black, 
            
          ),
          
        ],
        backgroundColor: Colors.white,
      ),
      // body: ListView(
      //   children: [
      //     Column(
      //        mainAxisAlignment: MainAxisAlignment.start,
      //        crossAxisAlignment: CrossAxisAlignment.start,
      //        children: [
      //          Container(
      //            //color: Colors.black,
      //            child: Image.network(
      //              "https://e0.pxfuel.com/wallpapers/827/998/desktop-wallpaper-virat-kohli-virat-kohli-batting.jpg",
      //              width: double.infinity,
      //              //height: 300,
                  
      //           ),
      //          ),
      //          Row(
      //             children: [
      //              IconButton(
      //                onPressed: (){
      //                 setState(() {
      //                   c1 = !c1;
      //                 });
      //                }, 
      //                icon: c1? const Icon(
      //                 Icons.favorite_rounded,
      //                 color: Colors.red,)
      //                   :const Icon(
      //                   Icons.favorite_outline,
      //                 ),
      //                ),
                   
      //              IconButton(
      //               onPressed: (){}, 
      //                icon: const Icon(
      //                  Icons.comment_outlined,
      //                ),
      //              ),
      //              IconButton(
      //                onPressed: (){}, 
      //                icon: const Icon(
      //                  Icons.send,
      //               ),
      //             ),
      //             const Spacer(),
      //             IconButton(
      //               onPressed: (){}, 
      //               icon: const Icon(
      //                 Icons.bookmark_outline_outlined,
      //               ),),
      //           ],
      //         )
      //       ],
      //     ),
      //   ],
      // ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,  
            children: [
              Container(
                //color: Colors.black,
                child: Image.network(
                  "https://e0.pxfuel.com/wallpapers/827/998/desktop-wallpaper-virat-kohli-virat-kohli-batting.jpg",
                  width: double.infinity,
                  //height: 300,
                  
               ),
              ),
              Row(
                children: [
                  IconButton(
                    onPressed: (){
                      setState(() {
                        liked1 = !liked1;
                      });
                    }, 
                    icon: liked1? const Icon(
                       Icons.favorite_rounded,
                       color: Colors.red,)
                         :const Icon(
                         Icons.favorite_outline,
                       ),
                    ),
                  
                  IconButton(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.comment_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.send,
                    ),
                  ),
                  const Spacer(),
                  IconButton(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.bookmark_border_sharp,
                    ),
                  ),
                ],
              )
            ],
          ),
            Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                //color: Colors.black,
                child: Image.network(
                  "https://e0.pxfuel.com/wallpapers/827/998/desktop-wallpaper-virat-kohli-virat-kohli-batting.jpg",
                  width: double.infinity,
                  //height: 300,
                  
                ),
              ),
              Row(
                children: [
                  IconButton(
                    onPressed: (){
                      setState(() {
                        liked2 = !liked2;
                      });
                    }, 
                    icon: liked2? const Icon(
                      Icons.favorite_rounded,
                      color: Colors.red,
                    ):const Icon(Icons.favorite_outline),
                  ),
                  IconButton(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.comment_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.send,
                    ),
                  ),
                  const Spacer(),
                  IconButton(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.bookmark_border_sharp,
                    ),
                  ),
                ],
              )
            ],
          ),
            Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                //color: Colors.black,
                child: Image.network(
                  "https://e0.pxfuel.com/wallpapers/827/998/desktop-wallpaper-virat-kohli-virat-kohli-batting.jpg",
                  width: double.infinity,
                  //height: 300,
                  
                ),
              ),
              Row(
                children: [
                  IconButton(
                    onPressed: (){
                      setState(() {
                        liked3 = !liked3;
                      });
                    }, 
                    icon: liked3? const Icon(
                      Icons.favorite_rounded,
                      color: Colors.red,
                    ):const Icon(Icons.favorite_outline),
                  ),
                  IconButton(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.comment_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.send,
                    ),
                  ),
                  const Spacer(),
                  IconButton(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.bookmark_border_sharp,
                    ),
                  ),
                ],
              )
            ],
          ),
          ],
        ),
      ),
    );
  }
}