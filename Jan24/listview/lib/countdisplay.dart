
import 'package:flutter/material.dart';

class CountDisplay extends StatefulWidget {
  const CountDisplay({super.key});

  @override
  State<CountDisplay> createState() => _CountDisplayState();
}

class _CountDisplayState extends State<CountDisplay> {

  List <int> countList = [];
  int _counter = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("List View Builder",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20,
          color: Colors.black,
          ),
        ),
        backgroundColor: Colors.pink,
      ),
      body: ListView.builder(
        itemCount: countList.length,
        itemBuilder: (context,index){
          return Container(
            margin: const EdgeInsets.only(top: 10,left: 10,right: 10),
            height: 50,
            width: double.infinity,
            color: Colors.orange,
            child:  Center(
              child: Text("${countList[index]}",// index-1 asto
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 25,
              ),),
            ),
            
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed:(){
          setState(() {
            // if(_counter < countList.length-1){
            _counter++;
            countList.add(_counter);
          });
        },
        child: const Text("Add",
        style: TextStyle(
          fontWeight: FontWeight.bold,
        ),),
      ),
    );
  }
}