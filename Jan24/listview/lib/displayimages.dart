
import 'package:flutter/material.dart';

class DisplayImages extends StatefulWidget {
  const DisplayImages({super.key});

  @override
  State<DisplayImages> createState() => _DisplayImagesState();
}

class _DisplayImagesState extends State<DisplayImages>{

  List<String> imageList = [
    "https://media.istockphoto.com/id/530741074/photo/red-fort-lal-qila-with-indian-flag-delhi-india.jpg?s=612x612&w=0&k=20&c=7BTI-dgQNPPTq2yARrwIBf2yIqO4PUPX1EJY5ITIyoM=",
    "https://vajiram-prod.s3.ap-south-1.amazonaws.com/Rajgad_Fort_2eaa0ca9a4.jpg",
    "https://upload.wikimedia.org/wikipedia/commons/d/de/Nagarkhana%2C_Raigad_Fort%2C_India.jpg",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("List View Builder Demo",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20,
          color: Colors.black,
          ),
        ),
        backgroundColor: Colors.pink,
      ),
      body: ListView.builder(
        itemCount: imageList.length,
        itemBuilder: (context,index){
          return Container(
            margin: const EdgeInsets.all(10),
            child: Image.network(
              imageList[index],
            ),
          );
        },
      ),
    );
  }
}