
import 'package:flutter/material.dart';

class AppDemo extends StatefulWidget {
  const AppDemo({super.key});

  @override
  State<AppDemo> createState() => _AppDemoState();
}

class _AppDemoState extends State<AppDemo> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("ListView Demo",
        style:TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20,
          color: Colors.black,
          ),
        ),
        backgroundColor: Colors.pink,
      ),
      body: ListView(
        children: [
          Container(
            margin: const EdgeInsets.all(10),
            child: Image.network(
              "https://media.istockphoto.com/id/530741074/photo/red-fort-lal-qila-with-indian-flag-delhi-india.jpg?s=612x612&w=0&k=20&c=7BTI-dgQNPPTq2yARrwIBf2yIqO4PUPX1EJY5ITIyoM=",
            ),
          ),
          Container(
            margin: const EdgeInsets.all(10),
            child: Image.network(
              "https://vajiram-prod.s3.ap-south-1.amazonaws.com/Rajgad_Fort_2eaa0ca9a4.jpg",
            ),
          ),
          Container(
            margin: const EdgeInsets.all(10),
            child: Image.network(
              "https://upload.wikimedia.org/wikipedia/commons/d/de/Nagarkhana%2C_Raigad_Fort%2C_India.jpg",
            ),
          ),
        ],
      ),
    );
  }
}