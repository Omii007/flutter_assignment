import 'package:flutter/material.dart';
import 'package:multiprovider_demo_app/views/get_product_details.dart';
import 'package:provider/provider.dart';

import 'controllers/product_controller.dart';
import 'controllers/wishlist_controller.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (BuildContext context) {
          return ProductController();
        }),
        ChangeNotifierProvider(
          create: (BuildContext context) {
            return WishListController();
          },
        ),
      ],
      builder: (context, child) {
        return MaterialApp(
          home: GetProductDetails(),
          title: "Provider Demo",
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(
              seedColor: Colors.deepPurple,
            ),
          ),
        );
      },
    );
  }
}
