import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:multiprovider_demo_app/controllers/product_controller.dart';
import 'package:multiprovider_demo_app/controllers/wishlist_controller.dart';
import 'package:provider/provider.dart';

class WishListScreen extends StatefulWidget {
  const WishListScreen({super.key});

  @override
  State createState() => _WishListScreenState();
}

class _WishListScreenState extends State<WishListScreen> {
  @override
  Widget build(BuildContext context) {
    final wishlistObj = Provider.of<WishListController>(context, listen: false);
    final productlistObj =
        Provider.of<ProductController>(context, listen: false);
    return Scaffold(
      body: ListView.builder(
        itemCount: wishlistObj.listOfWishlistProducts.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              Image.network(
                "${wishlistObj.listOfWishlistProducts[index].productImage}",
              ),
              const SizedBox(
                height: 10,
              ),
              Text("${wishlistObj.listOfWishlistProducts[index].productName}"),
              const SizedBox(
                height: 10,
              ),
              Text("${wishlistObj.listOfWishlistProducts[index].price}"),
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Consumer(builder: (context, provider, child) {
                    log("IN WISHLIST CONSUMER");
                    return GestureDetector(
                      onTap: () {
                        wishlistObj.removeWishList(index: index);
                      },
                      child: Icon(
                        productlistObj.listOfProduct[index].isFavorite
                            ? Icons.favorite_outlined
                            : Icons.favorite_outline_rounded,
                        color: Colors.red,
                      ),
                    );
                  }),
                  const Spacer(),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          productlistObj.addQuantity(index);
                        },
                        child: const Icon(
                          Icons.add,
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Consumer(builder: (context, provider, child) {
                        return Text(
                            "${productlistObj.listOfProduct[index].quantity}");
                      }),
                      const SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                        onTap: () {
                          productlistObj.removeQuantity(index);
                        },
                        child: const Icon(
                          Icons.remove,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
    );
  }
}
