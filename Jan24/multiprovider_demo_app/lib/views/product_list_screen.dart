import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:multiprovider_demo_app/controllers/product_controller.dart';
import 'package:multiprovider_demo_app/controllers/wishlist_controller.dart';
import 'package:provider/provider.dart';

import 'wishlist_screen.dart';

class ProductListScreen extends StatelessWidget {
  const ProductListScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final providerObj = Provider.of<ProductController>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const WishListScreen(),
                ),
              );
            },
            icon: const Icon(
              Icons.favorite_border_rounded,
              color: Colors.red,
            ),
          ),
        ],
        title: const Text("Product List Screen"),
      ),
      body: ListView.builder(
          itemCount: providerObj.listOfProduct.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              margin: const EdgeInsets.only(bottom: 50),
              child: Column(
                children: [
                  Image.network(
                      "${providerObj.listOfProduct[index].productImage}"),
                  const SizedBox(
                    height: 10,
                  ),
                  Text("${providerObj.listOfProduct[index].productName}"),
                  const SizedBox(
                    height: 10,
                  ),
                  Text("${providerObj.listOfProduct[index].price}"),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      /// IS FAVORITE
                      Consumer<ProductController>(
                        builder: (context, provider, child) {
                          log("IN FAV CONSUMER");
                          return GestureDetector(
                            onTap: () {
                              /// PRODUCT ADD TO WISHLIST
                              providerObj.addToFavorite(index: index);

                              if (providerObj.listOfProduct[index].isFavorite) {
                                Provider.of<WishListController>(context,
                                        listen: false)
                                    .addDataToWishlist(
                                        obj: providerObj.listOfProduct[index]);
                              }
                            },
                            child: Icon(
                              (providerObj.listOfProduct[index].isFavorite)
                                  ? Icons.favorite_rounded
                                  : Icons.favorite_outline_rounded,
                              color: Colors.red,
                            ),
                          );
                        },
                      ),
                      const Spacer(),
                      Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              providerObj.addQuantity(index);
                            },
                            child: const Icon(Icons.add),
                          ),
                          const SizedBox(
                            width: 5,
                          ),

                          /// QUANTITY
                          Consumer<ProductController>(
                            builder: (context, provider, child) {
                              log("IN QUANTITY CONSUMER");
                              return Text(
                                  "${providerObj.listOfProduct[index].quantity}");
                            },
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          GestureDetector(
                            onTap: () {
                              providerObj.removeQuantity(index);
                            },
                            child: const Icon(Icons.remove),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            );
          }),
    );
  }
}
