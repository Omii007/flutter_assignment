import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:multiprovider_demo_app/controllers/product_controller.dart';
import 'package:multiprovider_demo_app/models/product_model.dart';
import 'package:provider/provider.dart';

import 'product_list_screen.dart';

class GetProductDetails extends StatelessWidget {
  GetProductDetails({super.key});

  final TextEditingController _productNameController = TextEditingController();
  final TextEditingController _productImageController = TextEditingController();
  final TextEditingController _productPriceController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    log("IN PRODUCT DETAILS BUILD");
    return Scaffold(
      appBar: AppBar(
        title: const Text("GET PRODUCT DETAILS"),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            TextFormField(
              controller: _productImageController,
              decoration: InputDecoration(
                hintText: "Add Image",
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            TextFormField(
              controller: _productNameController,
              decoration: InputDecoration(
                hintText: "Enter Name",
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            TextFormField(
              controller: _productPriceController,
              decoration: InputDecoration(
                hintText: "Enter Price",
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () {
                final obj = ProductModel(
                  isFavorite: false,
                  price: _productPriceController.text.trim(),
                  productImage: _productImageController.text.trim(),
                  productName: _productNameController.text.trim(),
                  quantity: 1,
                );

                Provider.of<ProductController>(context, listen: false)
                    .addProductData(pObj: obj);
                _productImageController.clear();
                _productNameController.clear();
                _productPriceController.clear();
              },
              child: const Text("Add"),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const ProductListScreen(),
                  ),
                );
              },
              child: const Text("Submit"),
            ),
          ],
        ),
      ),
    );
  }
}
