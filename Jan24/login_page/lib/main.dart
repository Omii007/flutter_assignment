import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: LoginApp(),
      debugShowCheckedModeBanner:false,
    );
  }
}

class LoginApp extends StatefulWidget {
  const LoginApp({super.key});

  @override
  State createState()=> _LoginAppState();
}

class _LoginAppState extends State<LoginApp>{

  final TextEditingController _usernameTextEditingController = 
    TextEditingController();
  final TextEditingController _passwordTextEditingController = 
    TextEditingController();

  // KEYS
  // final GlobalKey<FormFieldState> _userNameKey = GlobalKey<FormFieldState>();
  // final GlobalKey<FormFieldState> _passwordNameKey = GlobalKey<FormFieldState>();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool flag = true;
  void showPassward(){
    setState(() {
      flag = !flag;
    });
  }

  @override
  Widget build(BuildContext context){

    return Scaffold(
      appBar: AppBar(
        title: const Text("Login Page",
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.w600,
            color: Color.fromRGBO(0, 0, 0, 1),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              Image.network(
                "https://w7.pngwing.com/pngs/340/946/png-transparent-avatar-user-computer-icons-software-developer-avatar-child-face-heroes-thumbnail.png",
                height: 100,
                width: 100,
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: _usernameTextEditingController,
                // key: _userNameKey,
                decoration: InputDecoration(
                  prefixIcon: const Icon(Icons.person),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  hintText: "Enter username",
                  label: const Text("Enter username"),
                ),
                validator:(value) {
                  if(value == null || value.trim().isEmpty){
                    return "Please enter username";
                  }else{
                    return null;
                  }
                },
                keyboardType: TextInputType.emailAddress,
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: _passwordTextEditingController,
                // key:_passwordNameKey,
                obscureText:flag,
                // obscuringCharacter:"*",
                decoration: InputDecoration(
                  suffixIcon: GestureDetector(
                    onTap: showPassward,                    
                    child: Icon(
                      flag? Icons.visibility_off : Icons.visibility),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  hintText: "Enter password",
                  label: const Text("Enter password"),
                ),
                validator: (value) {
                  if(value == null || value.trim().isEmpty){
                    return "Please enter password";
                  }else{
                    return null;
                  }
                },
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: (){
          
                  bool loginValidated = _formKey.currentState!.validate();
          
                  if(_usernameTextEditingController.text.trim().isEmpty ||
                    _passwordTextEditingController.text.trim().isEmpty
                  ){
                    return;
                  }else{
                    if(loginValidated){
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text("Login Successful"),
                        ),
                      );
                    }else{
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content:Text("Login Failed"),
                        ),
                      );
                    }
                  }
                  
                  // bool userNameValidated = _userNameKey.currentState!.validate();
                  // bool passwordValidated = _passwordNameKey.currentState!.validate();
          
                  // if(_usernameTextEditingController.text.trim().isEmpty || 
                  //     _passwordTextEditingController.text.trim().isEmpty
                  //   ){
                  //     return;
                  //   }else{
                  //   if(userNameValidated && passwordValidated){
                  //     ScaffoldMessenger.of(context).showSnackBar(
                  //       const SnackBar(content:  Text("Login Successful"),
                  //       ),
                  //     );
                  //   }else{
                  //     ScaffoldMessenger.of(context).showSnackBar(
                  //       const SnackBar(content: Text("Login Failed"),
                  //       ),
                  //     );
                  //   }
                  // }
                }, 
                child: const Text("Login"),
              )
            ],
          ),
        ),
      ),
    );
  }
}