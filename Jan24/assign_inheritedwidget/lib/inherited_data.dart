
import 'package:assign_inheritedwidget/login_screen.dart';
import 'package:flutter/material.dart';

class InheritedData extends InheritedWidget {
  
  final Employee employee;

  const InheritedData({
    super.key,
    required this.employee,
    required super.child,
  });

  @override
  bool updateShouldNotify(InheritedData oldWidget){
    return true;
  }

  static InheritedData of(BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<InheritedData>()!;
  }
}