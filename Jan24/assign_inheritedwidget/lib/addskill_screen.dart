
import 'package:flutter/material.dart';

class AddSkill extends StatefulWidget {
  const AddSkill({super.key});

  @override
  State createState()=> _AddSkillState();
}

class _AddSkillState extends State {

  TextEditingController skillController = TextEditingController();
  bool flag = true;
  var obj;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text("Add Skill",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25,
            color: Colors.black,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
          
              const SizedBox(
                  height: 100,
                ),
                // ID
                TextFormField(
                  controller: skillController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25),
                      borderSide: const BorderSide(
                        color: Colors.black,
                        width: 3,
                      ),
                    ),
                    hintText: "Enter employee skill",
                    hintStyle: const TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.normal,
                      color: Color.fromRGBO(0,0,0,0.7),
                    ),
                  ),
                ),
          
                const SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  onTap: () {
                    //obj = skillController.text;
                    setState(() {
                      flag = !flag;
                    });
                    //skillController.clear();
                  },
                  child: Container(
                    height: 50,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: Colors.blue,
                    ),
                    child:const Center(
                      child: Text("Add Skill",
                      textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                
                const SizedBox(
                  height: 20,
                ),
                const Divider(
                  color: Colors.black,
                  thickness: 2,
                ),
                const SizedBox(
                  height: 20,
                ),
                flag?
                Text(skillController.text,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Colors.black
                  ),
                ): const Text(""),
            ],
          ),
        ),
      ),
    );
  }
}