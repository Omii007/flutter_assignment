import 'package:assign_inheritedwidget/inherited_data.dart';
import 'package:assign_inheritedwidget/login_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return InheritedData(
      employee: Employee(
      ), 
      child: const MaterialApp(
        home: LoginScreen(),
        debugShowCheckedModeBanner: false,
      ));
  }
}
