
import 'package:assign_inheritedwidget/display_screen.dart';
import 'package:assign_inheritedwidget/inherited_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State createState()=> _LoginPageState();
}

class Employee{
  int? id;
  String? name;
  String? userName;

  Employee({
    this.id,
    this.name,
    this.userName,
  });
}

class _LoginPageState extends State {

  TextEditingController idController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController userNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    InheritedData inheritedData=InheritedData.of(context);
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            const Row(
              //mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 150,
                ),
                Text("Login",
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color:Colors.black,
                  ),
                ),
              ],
            ),
        
            const SizedBox(
              height: 0,
            ),
            // ID
            TextFormField(
              controller: idController,
              decoration: InputDecoration(
                enabledBorder:  OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25),
                  borderSide: const BorderSide(
                    color: Colors.black,
                    width: 3,
                  ),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25),
                  borderSide: const BorderSide(
                    color: Colors.black,
                    width: 3,
                  ),
                ),
                hintText: "Enter employee id",
                hintStyle: const TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.normal,
                  color: Color.fromRGBO(0,0,0,0.7),
                ),
              ),
            ),
        
            const SizedBox(
              height: 20,
            ),
        
            // NAME
            TextFormField(
              controller: nameController,
              decoration: InputDecoration(
                enabledBorder:  OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25),
                  borderSide: const BorderSide(
                    color: Colors.black,
                    width: 3,
                  ),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25),
                  borderSide: const BorderSide(
                    color: Colors.black,
                    width: 3,
                  ),
                ),
                hintText: "Enter employee name",
                hintStyle: const TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.normal,
                  color: Color.fromRGBO(0,0,0,0.7),
                ),
              ),
            ),
            
            const SizedBox(
              height: 20,
            ),
            // USERNAME
            TextFormField(
              controller: userNameController,
              decoration: InputDecoration(
                enabledBorder:  OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25),
                  borderSide: const BorderSide(
                    color: Colors.black,
                    width: 3,
                  ),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25),
                  borderSide: const BorderSide(
                    color: Colors.black,
                    width: 3,
                  ),
                ),
                hintText: "Enter employee username",
                hintStyle: const TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.normal,
                  color: Color.fromRGBO(0,0,0,0.7),
                ),
              ),
            ),
            const SizedBox(
              height: 40,
            ),
        
            GestureDetector(
              onTap: () {
                
                inheritedData.employee.id=int.parse(idController.text);
                inheritedData.employee.name = nameController.text;
                inheritedData.employee.userName = userNameController.text;

                Navigator.push(
                  context, 
                  MaterialPageRoute(
                    builder: (context)=> const DisplayData(),
                    ),
                  
                );
              },
              child: Container(
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                  color: Colors.blue,
                ),
                child:const Center(
                  child: Text("Submit",
                  textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}