
import 'package:flutter/material.dart';

class FlipkartApp extends StatefulWidget {
  const FlipkartApp({super.key});

  @override
  State<FlipkartApp> createState() => _FlipkartAppState();
}

class _FlipkartAppState extends State<FlipkartApp> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          const Padding(padding: EdgeInsets.only(left: 70),),
          Image.asset("assets/images/Flipkart-Logo.png",
          ),
          Container(
            padding: const EdgeInsets.only(right: 400),
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              color: Color.fromARGB(255, 222, 232, 239)
            ),
            margin: const EdgeInsets.all(10),
            height: 15,
            width: 700,
            child:  Center(
              child:  TextButton.icon(
                onPressed: (){}, 
                icon: const Icon(
                  Icons.search,
                  color: Colors.black45,
                ), 
                label: const Text(
                  "Search for Products, Brands and More ",
                  style: TextStyle(
                    color: Colors.black45,
                  ),
                ),
              ),
            ),
          ),
          const Padding(padding: EdgeInsets.only(left: 20),),
          TextButton.icon(
            onPressed: (){}, 
            icon: const Icon(Icons.person,
              size: 30,
              color: Colors.black,
            ), 
            label: const Text("Login",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w400,
                fontSize: 20,
              ),
            ),
          ),
          const Padding(padding: EdgeInsets.only(left: 20),),
          TextButton.icon(
            onPressed: (){}, 
            icon: const Icon(Icons.shopping_cart,
              size: 30,
              color: Colors.black,
            ), 
            label: const Text("Cart",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w400,
                fontSize: 20,
              ),
            ),
          ),
          const Padding(padding: EdgeInsets.only(left: 20),),
          TextButton.icon(
            onPressed: (){}, 
            icon: const Icon(Icons.storefront_outlined,
              size: 30,
              color: Colors.black,
            ), 
            label: const Text("Become a Seller",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w400,
                fontSize: 20,
              ),
            ),
          ),
          const Padding(padding: EdgeInsets.only(left: 35),),
          IconButton(
            onPressed: (){}, 
            icon: const Icon(Icons.more_vert,color: Colors.black87,),),
          const Spacer(),
          
        ],
        backgroundColor: Colors.white,
      ),
      
      body:  SingleChildScrollView(
        child: Column(
          children: [
            const Padding(padding: EdgeInsets.all(10)),
            Row(
              children: [
                const Padding(padding: EdgeInsets.all(10)),
                Container(
                  height: 130,
                  width: 1500,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Padding(padding: EdgeInsets.only(left: 50)),
                      Column(
                        children: [
                          
                          Image.asset(
                            "assets/images/Grocery.png",
                            height: 90,
                            width: 90,
                          ),
                          const SizedBox(
                            height: 1,
                          ),
                          const Text("Grocery",
                            style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Image.asset(
                            "assets/images/Electronics.png",
                            height: 90,
                            width: 90,
                          ),
                          const SizedBox(
                            height: 1,
                          ),
                          const Text("Electronics",
                            style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Image.asset(
                            "assets/images/Fashion.png",
                            height: 90,
                            width: 90,
                          ),
                          const SizedBox(
                            height: 1,
                          ),
                          const Text("Fashion",
                            style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Image.asset(
                            "assets/images/Home.png",
                            height: 90,
                            width: 90,
                          ),
                          const SizedBox(
                            height: 1,
                          ),
                          const Text("Home & Furniture",
                            style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Image.asset(
                            "assets/images/Appliances.png",
                            height: 90,
                            width: 90,
                          ),
                          const SizedBox(
                            height: 1,
                          ),
                          const Text("Appliances",
                            style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Image.asset(
                            "assets/images/Travel.png",
                            height: 90,
                            width: 90,
                          ),
                          const SizedBox(
                            height: 1,
                          ),
                          const Text("Travel",
                            style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Image.asset(
                            "assets/images/Beauty Toys.png",
                            height: 90,
                            width: 90,
                          ),
                          const SizedBox(
                            height: 1,
                          ),
                          const Text("Beauty, Toys & More",
                            style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Image.asset(
                            "assets/images/Two Wheelers.png",
                            height: 90,
                            width: 90,
                          ),
                          const SizedBox(
                            height: 1,
                          ),
                          const Text("Two Wheelers",
                            style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      const Padding(padding: EdgeInsets.only(right: 50)),
                    ],
                  ),
                ),  
              ],
            ),
          ],
        ),
      ),
    );
  }
}