
import 'package:flutter/material.dart';

class AppBar5 extends StatelessWidget {
  const AppBar5({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset("asset/images/DSC_1538.JPG",
                width: 500,
                height: 500,
              ),
              const SizedBox(
                width: 20,
              ),
              Image.asset("asset/images/DSC_9002.JPG",
                width: 500,
                height: 500,
              ),
              Image.asset("asset/images/Trikonafort.jpeg",
                height: 500,
                width: 500,
              ),
            ],
          ),
        ),
      ),
    );
  }
}