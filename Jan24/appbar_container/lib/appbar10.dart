
import 'package:flutter/material.dart';

class AppBar10 extends StatelessWidget {
  const AppBar10({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 300,
              height: 300,
              decoration: const BoxDecoration(
                color: Colors.pink,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20), 
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}