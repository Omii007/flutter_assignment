
import 'package:flutter/material.dart';

class AppBar2 extends StatelessWidget {
  const AppBar2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child:  Text(
            "AppBar2",
            style: TextStyle(
              color: Colors.black,
            ),),
        ),
        backgroundColor: Colors.blueGrey,
        actions:const [
          Icon(Icons.search),
          SizedBox(
            width: 15,
          ),
          Icon(Icons.person_2),
          SizedBox(
            width: 5,
          ),
        ],
      ),
    );
  }
}