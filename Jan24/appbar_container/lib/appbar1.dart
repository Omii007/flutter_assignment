
import 'package:flutter/material.dart';

class AppBar1 extends StatelessWidget{
  const AppBar1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "AppBar1",
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: Colors.red,
        actions: const [
          Icon(Icons.person),
          SizedBox(
            width: 15,
          ),
          Icon(Icons.home),
          SizedBox(
            width: 5,
          ),
        ],
      ),
    );
  }
}