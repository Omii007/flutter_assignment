import 'package:appbar_container/appbar10.dart';
import 'package:appbar_container/appbar2.dart';
import 'package:appbar_container/appbar3.dart';
import 'package:appbar_container/appbar4.dart';
import 'package:appbar_container/appbar5.dart';
import 'package:appbar_container/appbar6.dart';
import 'package:appbar_container/appbar7.dart';
import 'package:appbar_container/appbar8.dart';
import 'package:appbar_container/appbar9.dart';
import 'package:flutter/material.dart';

import 'appbar1.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: AppBar10(),
      debugShowCheckedModeBanner: false,
    );
  }
}
