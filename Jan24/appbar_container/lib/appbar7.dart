
import 'package:flutter/material.dart';

class AppBar7 extends StatelessWidget {
  const AppBar7({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Padding(padding: EdgeInsets.all(8.0)),
              Image.network(
                "https://wallpaper.dog/large/17293149.jpg",
                width: 500,
                height: 500,
              ),
              const Padding(padding: EdgeInsets.all(8.0)),
              Image.network(
                "https://wallpapers.com/images/hd/virat-kohli-of-indian-cricket-n1xdggrugp22uydl.jpg",
                width: 500,
                height: 500,
              ),
              const Padding(padding: EdgeInsets.all(8.0)),
              Image.network(
                "https://m.economictimes.com/thumb/msid-103628152,width-1600,height-900,resizemode-4,imgsize-137834/indian-team-members-celebrate-the-dismissal-of-sri-lankas-dasun-shanaka-during-.jpg",
                width: 500,
                height: 500,
              ),
              const Padding(padding: EdgeInsets.all(8.0)),
              Image.network(
                "https://staticg.sportskeeda.com/editor/2019/03/41dad-15520578741679-800.jpg",
                width: 412,
                height: 500,
              ),
              const Padding(padding: EdgeInsets.all(8.0)),
              Image.network(
                "https://images.hindustantimes.com/rf/image_size_960x540/HT/p2/2019/02/13/Pictures/india-vs-west-indies-cricket-match_d56798d8-2f4b-11e9-8feb-c7253ea4083e.jpg",
                width: 500,
                height: 500,
              ),
            ],
          ),
        ),
      ),
    );
  }
}