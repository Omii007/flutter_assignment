
import 'package:flutter/material.dart';

class AppBar6 extends StatelessWidget {
  const AppBar6({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.orange,
                  margin: const EdgeInsets.all(8.0),
                ),
                Container(
                  width: 100,
                  height: 100,
                  color: Colors.red,
                  margin: const EdgeInsets.all(8.0),
                ),
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.blueGrey,
                  margin: const EdgeInsets.all(8.0),
                ),
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.blue,
                  margin: const EdgeInsets.all(8.0),
                ),
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.green,
                  margin: const EdgeInsets.all(8.0),
                ),
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.pink,
                  margin: const EdgeInsets.all(8.0),
                ),
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.purple,
                  margin: const EdgeInsets.all(8.0),
                ),
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.blueAccent,
                  margin: const EdgeInsets.all(8.0),
                ),
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.deepOrange,
                  margin: const EdgeInsets.all(8.0),
                ),
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.deepPurple,
                  margin: const EdgeInsets.all(8.0),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}